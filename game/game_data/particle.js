particles = [];
function newParticle(x,y,dx,dy,ttl,options) {
  var options = options ? options : {};
  var p = {
    x:x,
    y:y,
    dx:dx,
    dy:dy,
    spin: ((Math.random()*2-1)>0 ? 1 : -1)*(5+10*Math.random()),
    segs: 3+Math.floor(Math.random()*2),
    ttl: ttl,
    age: 0,
    colors: options.colors ? options.colors : ["grey"]
  }
  particles.push(p);
}
function dparticle(x,y,r) {
  newParticle(
    x,
    y,
    Math.cos(r)*150,
    Math.sin(r)*150,
    Math.random()
  );
}
function rparticle(x,y,r) {
  newParticle(
    x,
    y,
    Math.cos(r)*300,
    Math.sin(r)*300,
    Math.random()/4,
    {
      colors: ["gray","white","yellow","orange","gray","gray","gray"]
    }
  );
}
function bparticle(x,y,r) {
  newParticle(
    x,
    y,
    Math.random()*Math.cos(r)*600,
    Math.random()*Math.sin(r)*600,
    Math.random()/4,
    {
      colors: ["white","lightgray","gray","gray"]
    }
  );
}
function explodeParticles(x,y,count,minAge,maxAge,minV,maxV) {
  for (var p = 0; p<count; p++){
    var a = Math.random()*Math.PI*2;
    var v = minV+Math.random()*(maxV-minV);
    newParticle(
      x,
      y,
      Math.cos(a)*v,
      Math.sin(a)*v,
      minAge+Math.random()*(maxAge-minAge),
      {
        colors: ["white","yellow","orange","gray","gray","gray","gray"]
      }
    )
  }
}


function updateParticles(dt) {
  var i = 0;
  while (i<particles.length){
    var p = particles[i];
    if (p.age>=p.ttl) {// condition to remove
      particles.splice(i,1)
    } else {
      p.age = Math.min(p.age+dt,p.ttl);
      p.dx -= p.dx*dt
      p.dy -= p.dy*dt
      p.x += p.dx*dt;
      p.y += p.dy*dt;
      i++;
    }
  }
}

function drawParticles() {
  for (var i = 0; i<particles.length; i++){
    var p = particles[i];
    ctx.fillStyle = p.colors[Math.floor(Math.max(0,Math.min(1,(p.age/p.ttl)))*(p.colors.length-1))];
    //console.log(Math.floor(Math.max(0,(p.age/p.ttl))*p.colors.length-1));
    var s = (p.ttl-p.age)/p.ttl;
    //circle(p.x,p.y,s*8+2,p.segs,p.spin*time);
    circle(p.x,p.y,s*8+2);
  }
}
