var enemies = [];
function newEnemy(x,y,options) {
  var options = options ? options : {};
  var enemy = {
    x: x,
    y: y,
    dx: 0,
    dy: 0,
    pdx: 0,
    pdy: 0,
    pfric: 10,
    hit: false,
    hitby: "none",
    wave: waveNumber,
    appear: 0,
    appearRate: 0.7+Math.random()*0.6,
    time: Math.floor(time*100)/100,
    name: options.name ? options.name : "Charlie",
    segments: options.segments ? options.segments : 20,
    hp: options.hp ? options.hp : 5,
    inithp: options.hp ? options.hp : 5,
    speed: options.speed ? options.speed : 50,
    size: options.size ? options.size : 20,
    color: options.color ? options.color : "red",
    spinnyAmp: options.spinnyAmp ? options.spinnyAmp : 0,
    spinnyRate: options.spinnyRate ? options.spinnyRate : 0,
    spinnyPhase: Math.random()*Math.PI*2,
    attackness: options.hasOwnProperty("attackness") ? options.attackness : 1,
    dodgeness: options.dodgeness ? options.dodgeness : 0,
    lerpness: options.lerpness ? options.lerpness : 10
  }
  enemies.push(enemy);
  return enemy;
}

enemySpawns = {}
enemySpawns.blackGoon = function(x,y) {
  var options = {
    color: "black",
    name: "Goon",
    hp: 3,
    size: 20,
    speed: 150,
    segments: 5,
    dodgeness: 0,
    attackness: 1,
    spinnyRate:(Math.random()>0.5 ? 1 : -1)*(0.5+Math.random()*0.5)
  }
  newEnemy(x,y,options);
}
enemySpawns.patienceGray = function(x,y) {
  var options = {
    color: "gray",
    name: "Patience",
    hp: 10,
    size: 60,
    segments: 7,
    lerpness: 0,
    dodgeness: 0,
    attackness: 0,
    spinnyAmp: 0,
    spinnyRate:(Math.random()>0.5 ? 1 : -1)*(0.5+Math.random()*0.5)
  }
  newEnemy(x,y,options);
}
enemySpawns.bigred = function(x,y) {
  var options = {
    name: "Big",
    hp: 10,
    size: 40,
    segments: 8,
    lerpness: 2,
    dodgeness: 5,
    spinnyAmp: 0.1,
    spinnyRate:(Math.random()>0.5 ? 1 : -1)*(2+Math.random()*2)
  }
  newEnemy(x,y,options);
}
enemySpawns.spinnyBlue = function(x,y) {
  var options = {
    hp: 1,
    speed: 75,
    size: 13,
    segments: 4,
    color: "cyan",
    name: "Spinny",
    spinnyAmp: 2+Math.random()*2,
    spinnyRate:(Math.random()>0.5 ? 1 : -1)*(3+Math.random()*10),
    attackness: 0.5,
    dodgeness: -5
  }
  newEnemy(x,y,options);
  newEnemy(x+1,y,options);
  newEnemy(x,y+1,options);
}
enemySpawns.tealTear = function(x,y) {
  var options = {
    hp: 4,
    speed: 400,
    size: 20,
    segments: 6,
    color: "purple",
    name: "Tear",
    spinnyRate:(Math.random()>0.5 ? 1 : -1)*(1+Math.random()*2),
    attackness: 1,
    lerpness: 1
  }
  newEnemy(x,y,options);
  //newEnemy(x+1,y,options);
  //newEnemy(x,y+1,options);
}
enemySpawns.orangeKamekaze = function(x,y) {
  var options = {
    hp: 1,
    speed: 200,
    size: 12,
    segments: 5,
    color: "orange",
    name: "Kamekaze",
    spinnyRate:(Math.random()>0.5 ? 1 : -1)*(3+Math.random()*10),
    attackness: 1,
    dodgeness: -50,
    lerpness: 10
  }
  newEnemy(x,y,options);
  newEnemy(x+1,y,options);
  newEnemy(x,y+1,options);
  newEnemy(x+1,y+1,options);
  newEnemy(x-1,y+1,options);
}
enemySpawns.yellowBastard = function(x,y) {
  var options = {
    hp: 3,
    speed: 150,
    size: 15,
    segments: 3,
    color: "yellow",
    name: "Bastard",
    spinnyAmp: 0.25,
    spinnyRate:(Math.random()>0.5 ? 1 : -1)*(2+Math.random()*7),
    dodgeness: 6,
    lerpness: 20,
    attackness: 0.5
  }
  newEnemy(x,y,options);
}

function updateEnemies(dt) {
  var i = 0;
  while (i<enemies.length){
    var enemy = enemies[i];
    if (enemy.hp<=0) {// condition to remove
      tunnelRate+=enemy.size/10;
      //saveImage = true;

      var killstat = {};
      killstat.n = enemy.name; // Name of the killed enemy
      killstat.h = enemy.hitby.tag; // Name of the weapon used
      killstat.u = gunpickups.length==0; // Was this kill "useful"? (Does it contribute to spawning weapons?)
      if (enemy.hitby.prefix != "")
        killstat.v = enemy.hitby.prefix; // Variant used ("any")
      if (enemy.hitby.hasOwnProperty("chain"))
        killstat.c = enemy.hitby.chain; // Order in chain for multi-hit weapons (bouncer, virus, ghost)
      stats.kills.push(killstat);

      sinceLast++;
      
      if (gunpickups.length==0 && sinceLast==15){
        while (gunpickups.length == 0){
          var a = Math.random()*Math.PI*2;
          var d = Math.sqrt(Math.random())*250;
          var gx = 400+Math.cos(a)*d;
          var gy = 300+Math.sin(a)*d;
          var dx = gx-player.x;
          var dy = gy-player.y;
          var d = Math.sqrt(dx*dx+dy*dy);
          if (d>150){
            newGunPickup(gx,gy);
          }
        }
      }
      
      kills++;
      shake+=0.05*enemy.inithp;
      sound.hit.play()
      // kills += 1;
      // kills = kills + 1;
      enemies.splice(i,1)

      explodeParticles(
        enemy.x,
        enemy.y,
        1.5*enemy.size*effects.enemyDieParticles,
        0,
        0.5,
        2*enemy.size,
        15*enemy.size
      )

    } else {
      var dx = player.x - enemy.x;
      var dy = player.y - enemy.y;
      var d = Math.sqrt(dx*dx+dy*dy);
      if (d<(enemy.size)){
        var ded = true;
        
        music.pause()
        sound.pulse.play();
        pulse.active = true;
        pulse.r = 0;
        pulse.x = enemy.x;
        pulse.y = enemy.y;
        music.currentTime = 0;
        over = true;
        sinceLast = 0;
        
        gunpickups = [];
        
        if (ded){
          stats.death={
            n: enemy.name,
            wep: player.current,
            t: Math.floor(time*100)/100,
            es: enemy.time,
            w: waveNumber
          };
        }
      }

      var nx = dx/d;
      var ny = dy/d;

      var mx = enemy.attackness*nx;
      var my = enemy.attackness*ny;

      for (var j=0; j<enemies.length; j++){
        var check = enemies[j];
        if (check!=enemy){
          var ex = check.x - enemy.x;
          var ey = check.y - enemy.y;
          var de = Math.sqrt(ex*ex+ey*ey);
          if (de<enemy.size+check.size){
            mx -= (ex/de);
            my -= (ey/de);
            enemy.x -= (ex/de);
            enemy.y -= (ey/de);
          }
        }
      }
      var dodgex = 0;
      var dodgey = 0;
      for (var j=0; j<bullets.length; j++){
        var check = bullets[j];
        var ex = check.x - enemy.x;
        var ey = check.y - enemy.y;
        var de = Math.sqrt(ex*ex+ey*ey);
        if (de<5*enemy.size){
          dodgex -= (ex/de)*enemy.dodgeness;
          dodgey -= (ey/de)*enemy.dodgeness;
        }
      }
      var dodged = Math.sqrt(dodgex*dodgex+dodgey*dodgey);
      if (dodged>0){
        mx += dodgex/dodged;
        my += dodgey/dodged;
      }

      mx += enemy.spinnyAmp * Math.cos(enemy.spinnyRate*time+enemy.spinnyPhase);
      my += enemy.spinnyAmp * Math.sin(enemy.spinnyRate*time+enemy.spinnyPhase);

      enemy.dx = lerp(enemy.dx,mx,dt*enemy.lerpness);
      enemy.dy = lerp(enemy.dy,my,dt*enemy.lerpness);
      enemy.pdx -= enemy.pdx*dt*enemy.pfric;
      enemy.pdx -= enemy.pdx*dt*enemy.pfric;

      enemy.x += (enemy.dx*enemy.speed + enemy.pdx)*dt;
      enemy.y += (enemy.dy*enemy.speed + enemy.pdy)*dt;
      enemy.x = Math.max(0,Math.min(enemy.x,800));
      enemy.y = Math.max(0,Math.min(enemy.y,600));
      enemy.appear+= dt * 2 * enemy.appearRate;
      i++;
    }
  }
}

function drawEnemies() {
  for (var i=0; i<enemies.length; i++){
    var enemy = enemies[i];
    var segs = enemy.segments;
    if (effects.enemySpawnPulse && enemy.appear<1){
      var w  = Math.max(1-enemy.appear,0)*enemy.size;
      ctx.strokeStyle = enemy.color;
      ctx.lineWidth = w;
      linecircle(enemy.x,enemy.y,enemy.appear*enemy.size*3,segs,-enemy.spinnyRate*shaketime+enemy.spinnyPhase);
    }
    enemy.hit = enemy.hit && effects.enemyHitFlash;
    if (enemy.hp>0 && !enemy.hit){
      ctx.strokeStyle = enemy.color;
      ctx.lineWidth = lerp(0,enemy.size,enemy.hp/enemy.inithp);
      linecircle(enemy.x,enemy.y,lerp(enemy.size,enemy.size/2,enemy.hp/enemy.inithp),segs,-enemy.spinnyRate*shaketime+enemy.spinnyPhase);
    } else if (effects.enemyHitFlash) {
        enemy.hit = false;
        ctx.fillStyle = "white";
        circle(enemy.x,enemy.y,enemy.size,segs,-enemy.spinnyRate*shaketime+enemy.spinnyPhase);
    }
  }
}
