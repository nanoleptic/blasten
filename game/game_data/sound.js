var sound = {}
var cur_playing = []

function setGlobalVolume(volume) {
  for (var snd in sound) {
    if (sound.hasOwnProperty(snd)) {
      if (sound[snd].global){
        sound[snd].setVolume(sound[snd].volume*volume);
      }
    }
  }
  for (var i = 0; i < cur_playing.length; i++) {
    if (cur_playing[i].global){
        cur_playing[i].setVolume(cur_playing[i].volume*volume);
    }
  }
}

function newSound(name, volume) {
  var elem = document.createElement("audio");
  elem.src = name+".wav";
  elem.id = name;
  elem.preload = true;
  elem.volume = + ((volume!=undefined) ? volume : 1);
  document.body.appendChild(elem);
  sound[name] = {
    //audio: new Audio(name+".wav"),
    play: function() {
      if (settings.sound){
        elem.currentTime = 0;
        elem.play();
      }
    },
    setVolume: function(volume){
      elem.volume = + ((volume!=undefined) ? volume : 1);
    },
    volume: (volume!=undefined) ? volume : 1,
    global: true
  }
}

newSound("die",0.5);
newSound("hit",0.25);
newSound("pulse");
sound.pulse.global = false;
newSound("pulse2");
sound.pulse2.global = false;
newSound("shoot1",0.5);
newSound("shoot2",0.5);
newSound("reload",0.5);
newSound("shoot3",0.5);
newSound("shoot4",0.75);
newSound("shoot5",0.75);
newSound("rocket",1);
newSound("bounce",0.75);
newSound("vshot",1);
newSound("virused",0.5);
newSound("vsplod",0.5);
newSound("splod",0.5);
newSound("thump",0.25);
