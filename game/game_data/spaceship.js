function newSpaceship() {
  return {
    x: 200,
    y: 200,
    dx: 0,
    dy: 0,
    rdx: 0,
    rdy: 0,
    accel: 2000,
    fric: 6,
    invulns: 0,
    bomb: false,
    invulnTime: 0,
    shotcool: 0,
    current: "null",
    trail: [],
    trailcool: 0.005
  }
}

doRecoil = true;
function recoil(p,a,f) {
  if (doRecoil){
    p.rdx += Math.cos(a+Math.PI)*f;
    p.rdy += Math.sin(a+Math.PI)*f;
  }
}

weapons = {}
weapons.null = function(x,y,angle,prefix) {
  return 0;
}
lefty = false
weapons.cannon = function(x,y,angle,prefix) {
  recoil(player,angle,100)
  qshake+=0.1;
  sound.shoot3.play()
  var options = {
    prefix: prefix ? prefix : "",
    color: "yellow",
    tag: "cannon"
  }
  var fa = angle+=(Math.random()*2-1)*0.1*Math.random()
  lefty = !lefty
  if (lefty){
    newBullet(x+Math.sin(fa)*10,y-Math.cos(fa)*10,fa, 0, 800, options);
    newBullet(x+Math.sin(fa)*10,y-Math.cos(fa)*10,fa, 0, 800, options);
  } else {
    newBullet(x-Math.sin(fa)*10,y+Math.cos(fa)*10,fa, 0, 800, options);
    newBullet(x-Math.sin(fa)*10,y+Math.cos(fa)*10,fa, 0, 800, options);
  }
  return (0.09+Math.random()*0.02)/3*2;
}

function shortRot(fr, to) {
  return ((fr - to + 7*Math.PI) % (Math.PI*2)) - Math.PI;
}
function oShortRot(fr, to) {
  return ((fr - to + 7*Math.PI) % (Math.PI*2)) - Math.PI;
}

var orbits = []
var oddOrbits = false;
function drawOrbits(){
  oddOrbits = !oddOrbits;
  for (var i = 0; i < orbits.length; i++) {
    var orbit = orbits[i];
    if (orbit.fired){
      ctx.strokeStyle = "white";
      ctx.lineWidth = 150*(orbit.age*orbit.age);
      line(
        orbit.sx,
        orbit.sy,
        orbit.ex,
        orbit.ey
      )
      ctx.strokeStyle = "rgba(255,255,255,"+orbit.age+")";
      ctx.lineWidth = 250*(1-orbit.age);
      line(
        orbit.sx,
        orbit.sy,
        orbit.ex,
        orbit.ey
      )
    } else if (oddOrbits) {
      ctx.strokeStyle = "rgba(0,0,0,0.25)";
      ctx.lineWidth = 50+100*(orbit.age*orbit.age);
      line(
        orbit.sx,
        orbit.sy,
        orbit.ex,
        orbit.ey
      )
    }
  }
}
function updateOrbits(dt) {
  for (var i = 0; i < orbits.length; i++) {
    var orbit = orbits[i];
    if (orbit.fired){
      orbit.age-=5*dt;
    } else {
      orbit.age += dt;
      if (orbit.age>1){
        shake += 1;
        orbit.age = 1;
        orbit.fired = true;
        for (var i = 0; i < enemies.length; i++) {
          var enemy  = enemies[i];
          if (pointLineDistance(orbit.sx, orbit.sy, orbit.ex, orbit.ey, enemy.x, enemy.y)<(enemy.size+75)) {
            enemy.hp = 0;
            enemy.hitby = orbit;
          }
        }
      }
    }
  }
  orbits = orbits.filter(function(orbit){
    return (!orbit.fired) || (orbit.age>0);
  })
}

function pointLineDistance(x1, y1, x2, y2, x0, y0){
  var num = (y2-y1)*x0 - (x2-x1)*y0 + x2*y1 - y2*x1;
  var denom = (y2 - y1)*(y2 - y1) + (x2 - x1)*(x2 - x1);
  return Math.abs(num) / Math.sqrt(denom);
}

weapons.orbital = function(x,y,angle,prefix) {
  qshake += 0.1;
  orbits.push({
    sx: x + Math.cos(angle)* 2000,
    sy: y + Math.sin(angle)* 2000,
    ex: x - Math.cos(angle)* 2000,
    ey: y - Math.sin(angle)* 2000,
    age: 0,
    color: "black",
    fired: false,
    tag: "orbital",
    prefix: prefix ? prefix : "",
  });
  return 1.25;
}

weapons.rocket = function(x,y,angle,prefix) {
  recoil(player,angle+Math.PI,50)
  sound.rocket.play()
  var splodoptions = {
    prefix: prefix ? prefix : "",
    color: "orange",
    shape: "circle",
    slowdown: 8,
    size: 12,
    tag: "rocketblast"
  }
  var options = {
    prefix: prefix ? prefix : "",
    tag: "rocket",
    color: "grey",
    size: 7,
    slowdown: -1.5,
    onupdate: function(b,dt){
      if (!b.hasOwnProperty("da")){
        b.da = 0;
      }
      var rotDir = shortRot(b.a,angle+Math.PI)>0 ? 1 : -1;
      b.da = lerp(b.da,rotDir*2,3*dt);
      b.a = b.a+b.da*dt*5;
      if (effects.rocketParticles && Math.random()>0.25)
        rparticle(b.x,b.y,b.a+Math.PI+(Math.random()*2+1)*0.2)
    },
    onhit: function(b) {
      sound.splod.play()
      qshake+=0.5;
      for (var i=0; i<40; i++)
        newBullet(b.x,b.y,Math.random()*Math.PI*2*(i/20), 0.3, 200+Math.random()*1500, splodoptions);
    }
  }
  newBullet(x,y,angle+Math.PI+Math.PI/2, Math.PI/2, 100+Math.random()*50, options);
  newBullet(x,y,angle+Math.PI-Math.PI/2, Math.PI/2, 100+Math.random()*50, options);
  return 0.3;
}
weapons.shield = function(x,y,angle,prefix) {
  //recoil(player,angle,50)
  sound.rocket.play();
  var options = {
    prefix: prefix ? prefix : "",
    tag: "shield",
    //shape: "circle",
    //color: "white",
    size: 7,
    slowdown: -0.01,
    onupdate: function(b,dt){
      if (
        (true || !b.hasOwnProperty("target")) ||
        b.target.hp<=0){
          if (enemies.length>0){
            var maxd = Infinity;
            for (var i = 0; i < enemies.length; i++) {
              var e = enemies[i];
              var dx = player.x-e.x;
              var dy = player.y-e.y;
              var d = dx*dx+dy*dy;
              if (d<maxd){
                b.target = e;
                maxd = d;
              }
            }
          } else {
            delete(b.target)
          }
      }
      if (b.hasOwnProperty("target")){
        var dx = b.target.x-b.x;
        var dy = b.target.y-b.y;
        var dir = Math.atan2(dy,dx);
        if (!b.hasOwnProperty("da")){
          b.da = 0;
        }
        var rotDir = shortRot(b.a,dir)/2//>0 ? 1 : -1;
        b.da = lerp(b.da,dir,dt*4);
        b.a = dir//b.a+b.da*dt*10;
      } else {
        //b.speed-=b.speed*2*dt
      }
    }
  }
  newBullet(x,y,angle,0.5, 500+Math.random()*50, options)
  return 0.01;
}
weapons.flak = function(x,y,angle,prefix) {
  recoil(player,angle,400)
  sound.shoot4.play()
  qshake+=0.3;
  var splodoptions = {
    prefix: prefix ? prefix : "",
    tag: "flakblast",
    color: "orange",
    shape: "circle",
    slowdown: 8,
    size: 12,
    onhit: function(b,e){
      if (b.hasOwnProperty("col") && b.col==e){
        //b.purge = false;
        b.hit = false;
        e.hp++;
        return;
      }
    }
  }
  var options = {
    prefix: prefix ? prefix : "",
    tag: "flak",
    color: "orange",
    slowdown: 2,
    onhit: function(bu, en) {
      clearTimeout(bu.timeout);
      qshake+=0.3;
      bu.sploded = "hello";
      bu.purge = true;
      sound.splod.play()
      for (var i=0; i<60; i++){
        var pellet = newBullet(bu.x,bu.y,bu.a+Math.random()*2-1+Math.PI, 0.3, 200+Math.random()*1500, splodoptions);
        pellet.col = en;
      }
      for (var i=0; i<10; i++){
        var pellet = newBullet(bu.x,bu.y,bu.a+Math.random()*2-1, 0.3, 200+Math.random()*1500, splodoptions);
        //pellet.col = en;
      }
    }
  }
  var myb = newBullet(x,y,angle, 0.1, 800+Math.random()*50, options);
  myb.timeout = setTimeout(function() {
    if (!myb.purge){
      qshake+=0.3;
      for (var i=0; i<60; i++){
        sound.splod.play()
        var pellet = newBullet(myb.x,myb.y,myb.a+Math.random()*2-1, 0.3, 200+Math.random()*1500, splodoptions);
      }
      for (var i=0; i<10; i++){
        sound.splod.play()
        var pellet = newBullet(myb.x,myb.y,myb.a+Math.PI+Math.random()*2-1, 0.3, 200+Math.random()*1500, splodoptions);
      }
    }
    myb.purge = true;
  },(350+Math.random()*50)/access.timescale);

  return 0.2;
}

weapons.bouncer = function(x,y,angle,prefix) {
  recoil(player,angle,100)
  sound.shoot5.play();
  qshake+=0.1;
  var options = {
    prefix: prefix ? prefix : "",
    tag: "bounce",
    color: "purple",
    slowdown: 0,
    //shape: "circle",
    size: 12,
    onhit: function(b, e){
      if (b.hasOwnProperty("col") && b.col==e){
        b.purge = false;
        b.hit = false;
        e.hp++;
        return;
      }
      qshake+=0.1;
      sound.bounce.play();
      var myop = options;
      var bounce = (+myop.tag.substr(7))+1;
      stats.bestbounce = Math.max(b.chain,stats.bestbounce);
      myop.rotate = Math.random()*2-1;
      var nb = newBullet(b.x,b.y,b.a+Math.PI+0.5, 1, 600, options);
      nb.chain = b.chain+1;
      nb.col = e;
      myop.rotate = Math.random()*2-1;
      nb = newBullet(b.x,b.y,b.a+Math.PI-0.5, 1, 600, options);
      nb.chain = b.chain+1;
      nb.col = e;
    }
  }
  var bb = newBullet(x,y,angle, 0, 600, options);
  bb.chain = 0;
  return 0.15;
}
weapons.fountain = function(x,y,angle,prefix) {
  var options = {
    prefix: prefix ? prefix : "",
    tag: "turret",
    color: "cyan",
    size: 12,
    shape: "circle",
    slowdown: 1.5
  }
  var nb = newBullet(x,y,angle, 0.15, 800+Math.random()*50, options);
  var nbshoot = function(){
    var b = nb;
    var dir;
    var options = {
      prefix: prefix ? prefix : "",
      color: "cyan",
      shape:circle,
      size: 5,
      slowdown:8,
      tag: "turretbullet"
    }
    sound.shoot1.play();
    var segs = 7;
    var off = Math.random()*Math.PI*2;
    for(var i=0;i<segs;i++){
      dir = off+b.a+Math.PI+i*(Math.PI*2)/segs;
      newBullet(b.x,b.y,dir, 0, 1100, options);
    }
    if (!nb.purge){
      setTimeout(nbshoot,(100/access.timescale));
    }
  }
  setTimeout(nbshoot,(100/access.timescale));
  return 0.5;
}
weapons.drone = function(x,y,angle,prefix) {
  var options = {
    prefix: prefix ? prefix : "",
    tag: "turret",
    color: "black",
    size: 16,
    shape: "line",
    rotate: (Math.random()*2-1)*0.5,
    onupdate: function(b,dt){
      if (effects.rocketParticles && Math.random()>0.25)
        rparticle(b.x,b.y,b.a+Math.PI+(Math.random()*2-1)*0.2)
    },
    slowdown: -0.2
  }
  var nb = newBullet(x,y,angle, 0.07, 200+Math.random()*50, options);
  nb.weap = Math.floor(Math.random()*weaponNames.length);
  var nbshoot = function(){
    var b = nb;
    if (
      (true || !b.hasOwnProperty("target")) || b.target.hp<=0){
        if (enemies.length>0){
          var maxd = Infinity;
          for (var i = 0; i < enemies.length; i++) {
            var e = enemies[i];
            var dx = b.x-e.x;
            var dy = b.y-e.y;
            var d = dx*dx+dy*dy;
            if (d<maxd){
              b.target = e;
              maxd = d;
            }
          }
        } else {
          delete(b.target)
        }
    }
    var dir;
    if (!b.purge){
      if (b.hasOwnProperty("target")){
        var dx = b.target.x-b.x;
        var dy = b.target.y-b.y;
        dir = Math.atan2(dy,dx);
        if (!nb.purge){
          doRecoil = false;
          setTimeout(nbshoot,(2000*weapons[weaponNames[b.weap]](b.x,b.y,dir,"turret"))/access.timescale);
          doRecoil = true;
        }
      } else {
        doRecoil = false;
        dir = Math.random()*Math.PI*2;
        setTimeout(nbshoot,(2000*weapons[weaponNames[b.weap]](b.x,b.y,dir,"turret"))/access.timescale);
        doRecoil = true;
      }
    }
  }
  for (var i=0; i<10; i++){
    var spr = (Math.random()*2-1);
    var options = {
      prefix: prefix ? prefix : "",
      tag: "scatter",
      color: "orange",
      shape: "circle",
      size: 14,
      slowdown: 5+Math.random()*4,
      rotate: spr * 2+Math.random()*2
    }
    newBullet(x,y,angle-spr*0.4, 0.1, 600+Math.random()*1000, options);
  }
  setTimeout(nbshoot,250/access.timescale);
  return 1;
}
weapons.scatter = function(x,y,angle,prefix) {
  recoil(player,angle,1000);
  qshake+=0.35;
  sound.shoot2.play()
  setTimeout(sound.reload.play,300/access.timescale);
  for (var i=0; i<40; i++){
    var spr = (Math.random()*2-1);
    var options = {
      prefix: prefix ? prefix : "",
      tag: "scatter",
      color: "orange",
      shape: "circle",
      size: 14,
      slowdown: 5+Math.random()*4,
      rotate: spr * 4+Math.random()*2
    }
    newBullet(x,y,angle-spr*0.8, 0.1, 200+Math.random()*2000, options);
  }
  return 0.5;
}
weapons.virus = function(x,y,angle,prefix) {
  recoil(player,Math.random()*Math.PI*2,200)
  qshake+=0.1;
  sound.vshot.play();
  var options = {
    prefix: prefix ? prefix : "",
    tag: "virus",
    color: "lightgreen",
    shape: "circle",
    size: 10,
    onhit: function(b, e){
      if (!b.hasOwnProperty("chain")){
        b.chain = 0;
      } else {
      }
      e.hp++;
      //e.dx += Math.cos(b.a)*(100/e.speed);
      //e.dy += Math.sin(b.a)*(100/e.speed);
      if (e.hasOwnProperty("mined"))
        return;
      sound.virused.play();
      e.color = "lightgreen";
      e.lerpness = 0;
      e.mined = true;
      var splodoptions = {
        prefix: prefix ? prefix : "",
        tag: "virus",
        color: "lightgreen",
        shape: "circle",
        slowdown: 8,
        onhit: options.onhit,
        size: 20
      }
      setTimeout(function() {
        if (e.hp>0){
          qshake+=0.6;
          e.hitby = b;
          sound.vsplod.play();
          e.hp = 0;
          for (var i=0; i<3*e.size; i++){
            var vb = newBullet(e.x,e.y,Math.random()*Math.PI*2*(i/20), 0.3, 500+Math.random()*2000, splodoptions);
            vb.chain = b.chain + 1;
          }
        }
      },250+(100*e.hp)/access.timescale)
    }
  }
  var vb = newBullet(x,y,angle, 0.2, 450,options);
  vb.chain = 0;
  //newBullet(x,y,angle+Math.PI/16, 0, 400,options);
  //newBullet(x,y,angle-Math.PI/16, 0, 400,options);
  return 0.1;
}
weapons.ghost = function(x,y,angle,prefix) {
  recoil(player,Math.random()*Math.PI*2,200)
  qshake+=0.1;
  sound.vshot.play()
  var options = {
    prefix: prefix ? prefix : "",
    tag: "ghost",
    color: "black",
    //shape: "circle",
    size: 12,
    onhit: function(b, e){
      b.purge = false;
      b.hit = false;
      if (!b.hasOwnProperty("cols")){
        b.cols = [];
      }
      var wascol = false;
      for (var i = 0; i < b.cols.length; i++) {
        if (b.cols[i]==e){
            e.hp++;
            //e.hit = false;
            wascol = true;
            break;
        }
      }
      if (!wascol){
        b.chain ++;
        sound.thump.play();
        b.cols.push(e);
        var gb = newBullet(b.x,b.y,b.a+(Math.random()*2-1)*0.25, 0, 700,b.options);
        gb.cols = b.cols.map(function(a){return a});
        gb.chain =b.chain
        b.a += (Math.random()*2-1)*0.25;
        //b.speed *= 0.75;
      }
    }
  }
  var gb = newBullet(x,y,angle, 0, 700,options);
  gb.chain = 0;
  //newBullet(x,y,angle+Math.PI/16, 0, 400,options);
  //newBullet(x,y,angle-Math.PI/16, 0, 400,options);
  return 0.09+Math.random()*0.02;
}
var chars = ['1','2','3','4','5','6','7','8','9','A','B','C','D','E','F'];
weapons.ojiro = function(x,y,angle,prefix) {
  recoil(player,angle,200)
  qshake+=0.2;
  sound.shoot1.play();
  var sx = Math.cos(angle) * 600;
  var sy = Math.sin(angle) * 600;
  var multi = 1;
  sx += player.dx * multi;
  sy += player.dy * multi;
  var a = Math.atan2(sy,sx);
  var s = lerp(Math.sqrt(sx*sx+sy*sy),600,0);
  var spr = Math.sqrt(player.dx*player.dx+player.dy*player.dy)*0.001;
  var options = {
    prefix: prefix ? prefix : "",
    tag: "ojiro",
    shape: "circle",
    rotate: shortRot(a, angle)*2,
    color: "red"
  }
  for (var i = 0; i < 3; i++) {
    newBullet(x,y,a, spr+0.05, s+200*Math.random(),options);
  }
  return 0.06+Math.random()*0.04;
}
var any = 0;
weapons.chaos = function(x,y,angle,prefix) {
  var coolMult = 0.5;
  any++;
  if (weaponNames[any%weaponNames.length]=="chaos")
    any++;
  return weapons[weaponNames[any%weaponNames.length]](x,y,angle,"any")*coolMult;
}
weaponNames = [
  //"shield",
  "cannon",
  "orbital",
  "scatter",
  "flak",
  "bouncer",
  "ghost",
  "rocket",
  "fountain",
  "virus",
  "ojiro",
  "drone",
  "chaos"
]
var aajoystick = {
  a: Math.random()*Math.PI*2,
  d: 0
}
var aatarget = {
  x: 400,
  y: 300
}
function updateSpaceship(ship,dt) {
  //ship.current = weaponNames[Math.floor(shotkills/15)%weaponNames.length]
  ship.shotcool = Math.max(ship.shotcool-dt,0);
  ship.invulnTime = Math.max(ship.invulnTime-dt,0);
  var ix = 0;
  var iy = 0;
  if (access.mousemove) {
    var tjoy = {
      a: aajoystick.a,
      d: aajoystick.d
    }
    var dx = mouseX - ship.x;
    var dy = mouseY - ship.y;
    var d = Math.sqrt(dx*dx+dy*dy);
    if (d>20){
        tjoy.d = 1;
        tjoy.a = Math.atan2(dy, dx);
    } else {
        tjoy.a = Math.atan2(-ship.dy, -ship.dx);
        tjoy.d = d/20;
    }
    var jx = lerp(Math.cos(aajoystick.a)*aajoystick.d, Math.cos(tjoy.a)*tjoy.d, 50*dt)
    var jy = lerp(Math.sin(aajoystick.a)*aajoystick.d, Math.sin(tjoy.a)*tjoy.d, 50*dt)
    
    var jd = Math.sqrt(jx*jx+jy*jy);
    aajoystick.d = jd;
    if (jd>0){
      aajoystick.a = Math.atan2(jy,jx);
    }
    
    ix = Math.cos(aajoystick.a)*aajoystick.d;
    iy = Math.sin(aajoystick.a)*aajoystick.d;
  } else if (access.automove) {
    var tjoy = {
      a: aajoystick.a,
      d: aajoystick.d
    }
    var target;
    var minDist = Infinity;
    for (var i = 0; i < enemies.length; i++) {
      var e = enemies[i];
      var dx = e.x-ship.x;
      var dy = e.y-ship.y;
      var d2 = dx*dx+dy*dy;
      if (d2<minDist){
        target = e;
        minDist = d2;
      }
    }
    if (Math.sqrt(minDist)<150){
      var dx = ship.x - target.x;
      var dy = ship.y - target.y;
      var d = Math.sqrt(dx*dx+dy*dy);
      tjoy.d = 1;
      tjoy.a = Math.atan2(dy, dx)+Math.PI/4*Math.sin(shaketime);
    } else if (gunpickups.length>0){
      target = gunpickups[0];
      var dx = target.x - ship.x;
      var dy = target.y - ship.y;
      var d = Math.sqrt(dx*dx+dy*dy);
      tjoy.d = 1;
      tjoy.a = Math.atan2(dy, dx);
    } else if (target!=undefined && Math.sqrt(minDist)>200){
      var dx = target.x - ship.x;
      var dy = target.y - ship.y;
      var d = Math.sqrt(dx*dx+dy*dy);
      tjoy.d = 1;
      tjoy.a = Math.atan2(dy, dx);
    } else {
      var dx = 400 - ship.x;
      var dy = 300 - ship.y;
      var d = Math.sqrt(dx*dx+dy*dy);
      tjoy.d = Math.min(1, d/1200);
      tjoy.a = Math.atan2(dy, dx);
    }
    var jx = lerp(Math.cos(aajoystick.a)*aajoystick.d, Math.cos(tjoy.a)*tjoy.d, 10*dt)
    var jy = lerp(Math.sin(aajoystick.a)*aajoystick.d, Math.sin(tjoy.a)*tjoy.d, 10*dt)
    
    var jd = Math.sqrt(jx*jx+jy*jy);
    aajoystick.d = jd;
    if (jd>0){
      aajoystick.a = Math.atan2(jy,jx);
    }
    
    ix = Math.cos(aajoystick.a)*aajoystick.d;
    iy = Math.sin(aajoystick.a)*aajoystick.d;
  } else if (gpstate.active){
    ix = gpstate.movement.x;
    iy = gpstate.movement.y;
  } else {
    var sq = true;
    if (keys[37] || keys[81] || keys[65] || keys[74]){
      sq = !sq;
      ix--;
    }
    if (keys[38] || keys[90] || keys[87] || keys[73]){
      sq = !sq;
      iy--;
    }
    if (keys[39] || keys[68] || keys[76]){
      sq = !sq;
      ix++;
    }
    if (keys[40] || keys[83] || keys[75]){
      sq = !sq;
      iy++;
    }
    if (sq){
      ix/=Math.sqrt(2);
      iy/=Math.sqrt(2);
    }
  }

  ship.dx += ship.accel*ix*dt-ship.dx*ship.fric*dt;
  ship.dy += ship.accel*iy*dt-ship.dy*ship.fric*dt;
  ship.rdx += ship.accel*ix*dt-ship.rdx*ship.fric*dt*2;
  ship.rdy += ship.accel*iy*dt-ship.rdy*ship.fric*dt*2;

  ship.x += (ship.rdx*0.5+ship.dx)*dt;
  ship.y += (ship.rdy*0.5+ship.dy)*dt;

  ship.trailcool -= dt;

  var t = 0;
  while(t<ship.trail.length){
    var seg = ship.trail[t];
    seg.age+=dt;
    if (seg.age>=seg.ttl){
      ship.trail.splice(t,1);
    } else {
      t++;
    }
  }
  //*
  if (ship.trailcool<0){
    var a = Math.atan2(ship.rdy*0.5+ship.dy,ship.rdx*0.5+ship.dx);
    ship.trail.push({
      x: ship.x,
      y: ship.y,
      a: a,
      w: 10,
      ttl: 0.2,
      age: 0
    })
  }
  //*/

  ship.x = Math.max(0,Math.min(canvas.width,ship.x))
  ship.y = Math.max(0,Math.min(canvas.height,ship.y))
  if (access.autoaim) {
    var target;
    var minDist = Infinity;
    for (var i = 0; i < enemies.length; i++) {
      var e = enemies[i];
      var dx = e.x-ship.x;
      var dy = e.y-ship.y;
      var d2 = dx*dx+dy*dy;
      if (d2<minDist){
        target = e;
        minDist = d2;
      }
    }
    
    if (target != undefined){
      var tp = fromto(ship.x, ship.y, target.x, target.y, 100);
      var tf = fromto(aatarget.x, aatarget.y, tp[0], tp[1], 300*dt);
      aatarget.x = tf[0];
      aatarget.y = tf[1];
    }
    aatarget.x += (ship.rdx*0.5+ship.dx)*dt;
    aatarget.y += (ship.rdy*0.5+ship.dy)*dt;
  }
  if (ship.shotcool==0){
    var doFire = mouseDown || keys[32] || gpstate.firing;
    if (access.autofire){
      doFire = !doFire;
    }
    if (doFire){
      var a;
      if (access.autoaim) {
        a = Math.atan2(aatarget.y-ship.y, aatarget.x-ship.x);
      } else if (gpstate.active){
        a = gpstate.aim;
      } else {
        a = Math.atan2(mouseY-ship.y,mouseX-ship.x);
      }
      //var a = Math.atan2(ship.dy,ship.dx)+Math.PI;
      ship.shotcool = weapons[ship.current](ship.x, ship.y, a);
      //tunnelRate-=0.2;
    }
  }
}
var drawScale = 1;
var lineOff = 35;
function drawSpaceship(ship) {

  ctx.fillStyle = "white";
  //ctx.strokeStyle = "black";
  ctx.lineWidth = 1;
  ctx.beginPath();
  //myMoveTo(player.x,player.y);
  for (var i = 0; i < ship.trail.length; i++) {
    var t = ship.trail[i]
    var w = drawScale * t.w*(t.ttl-t.age)/t.ttl;
    myLineTo(
      t.x+Math.cos(t.a+Math.PI/2)*w,
      t.y+Math.sin(t.a+Math.PI/2)*w
    )
  }
  for (var i = ship.trail.length-1; i >= 0; i--) {
    var t = ship.trail[i]
    var w = drawScale * t.w*(t.ttl-t.age)/t.ttl;
    myLineTo(
      t.x+Math.cos(t.a-Math.PI/2)*w,
      t.y+Math.sin(t.a-Math.PI/2)*w
    )
  }
  ctx.closePath()
  ctx.fill();
  //ctx.stroke();
  ctx.fillStyle = "white";
	circle(ship.x,ship.y,drawScale * 10,7,time);
  var blobs = 0;
  for (var i = 0; i <= blobs; i++) {
    var d = 30 + 10 * Math.cos(shaketime * 7);
    var a = shaketime * 3 + (i*Math.PI*2)/blobs;
    var dx = Math.cos(a)*d;
    var dy = Math.sin(a)*d;
    circle(ship.x+dx,ship.y+dy,6,7,time);
  }
  //ctx.strokeStyle = "white";
  //linecircle(ship.x, ship.y, 250, 50)
  //ctx.fillStyle = "black";
	//circle(ship.x,ship.y,10,7,time);
}

function shipBack(ship) {
  ctx.lineWidth = 1;
  ctx.strokeStyle = "grey";
  fline(ship.x+lineOff,ship.y,800,ship.y);
  fline(ship.x-lineOff,ship.y,0,ship.y);
  fline(ship.x,ship.y+lineOff,ship.x,600);
  fline(ship.x,ship.y-lineOff,ship.x,0);

  if (!(access.autoaim || gpstate.active)){
    var dx = ship.x-mouseX;
    var dy = ship.y-mouseY;
    var d2 = dx*dx+dy*dy;
    if (d2>(lineOff*lineOff*4)){
      var d = Math.sqrt(d2);
      var o1 = lineOff/d;
      var o2 = 1-o1;
      var x1 = lerp(ship.x,mouseX,o1);
      var y1 = lerp(ship.y,mouseY,o1);
      var x2 = lerp(ship.x,mouseX,o2);
      var y2 = lerp(ship.y,mouseY,o2);
      line(x1,y1,x2,y2);
    }
  }
}
