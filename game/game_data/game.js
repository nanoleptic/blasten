/*
			db    db  .d8b.  d8888b. d888888b  .d8b.  d8888b. db      d88888b .d8888.
			88    88 d8' `8b 88  `8D   `88'   d8' `8b 88  `8D 88      88'     88'  YP
			Y8    8P 88ooo88 88oobY'    88    88ooo88 88oooY' 88      88ooooo `8bo.
			`8b  d8' 88~~~88 88`8b      88    88~~~88 88~~~b. 88      88~~~~~   `Y8b.
			 `8bd8'  88   88 88 `88.   .88.   88   88 88   8D 88booo. 88.     db   8D
			   YP    YP   YP 88   YD Y888888P YP   YP Y8888P' Y88888P Y88888P `8888Y'
*/


var canvas = document.getElementById("myCanvas");
var gameElem = document.getElementById("game");
//var dcanvas = document.getElementById("dispCanvas");
var kb_ava = document.getElementById('kb_ava');
var cd_ava = document.getElementById('cd_ava');
var splashElements = document.getElementById("splash");
var scoreElements = document.getElementById("scores");
var scoreTextElements = document.getElementById("scoretext");
var claimScoreElements = document.getElementById("claimscore");
var ctx;
//var dctx;
var interval;
var kills = 0;
var shotkills = 0;
var player = newSpaceship();
var spawnIn = 10;
var time = 0;
var shake = 0;
var qshake = 0;
var recoilx = 0
var recoily = 0
var started = false;
var over = false;
var score = 0;
var stars = [];
var sentScore = false;
var mPitch = 1;
var stats;
var saveImage = false;
var gameName = "BLAST10";
var clickPlay = "Click to play (or press any button on a controller)"
var optionsMenuActive = false;
var pulse = {
	active:false,
	x:0,
	y:0,
	r:0,
	speed: 800
}
var drawcount = 0;
var lastdrawcount = 0;
var states = {
	options: "options",
	splash: "splash",
	menu: "menu",
	game: "game",
	dead: "dead",
}
var gstate=states.splash;
var global_dt = 0;

__DEBUG__ = false;
__GIF__ = false;

var effects = {
	extraParticles: true, // true
	rocketParticles: true, //true
	fireParticles: 3, // 3
	hitSmokeParticles: 5, // 5
	hitBurnParticles: 3, // 3
	enemyDieParticles: 1, //1
		enemyHitFlash: true, // true
		enemySpawnPulse: true, // true
		pickupPulse: true, // true
		bulletFlash: true, // true
		psychoTunnel: true, // true
		shakeAmount: 1, // 1
		weaponRainbow: true, //true
	lockCircles: false // false
}
var access = {
	autofire: false,
	autoaim: false,
	automove: false,
	mousemove: false,
	timescale: 1
}
var settings = {
	sound: true
}

if (__GIF__){
	var gif = new GIF({
	  workers: 2,
	  quality: 10,
		width: 800,
		height: 600
	});
	gif.on('finished', function(blob) {
	  window.open(URL.createObjectURL(blob));
	});
}


var sinceLast = 0;


/*
				d888888b d8b   db d888888b d888888b       dD Cb
				  `88'   888o  88   `88'   `~~88~~'     d8'   `8b
				   88    88V8o 88    88       88       d8       8b
				   88    88 V8o88    88       88      C88       88D
				  .88.   88  V888   .88.      88       V8       8P
				Y888888P VP   V8P Y888888P    YP        V8.   .8P
				                                          VD CP
*/

function init(){
	cgundisp = "";
	//splashElements.style.display = "block";
	//scoreElements.style.display = "none";
	ctx = canvas.getContext("2d");
	//dctx = dcanvas.getContext("2d");
	stats = {
		kills: [],
		wave: 0,
		weapons: []
	}
	lastGun = -1;
	kills = 0;
	shotkills = 0;
	player = newSpaceship();
	spawnIn = 10;
	time = 0;
	shake = 0;
	qshake = 0;
	recoilx = 0;
	recoily = 0;
	started = false;
	over = false;
	score = 0;
	stars = [];
	sentScore = false;
	mPitch = 1;

	pulse = {
		active:false,
		x:0,
		y:0,
		r:0,
		speed: 800
	}
	waveNumber = 0;
	timePerWave = 20;
	thisWave = 0;
	music.volume = 0;
	enemies = [];
	enemySpawns.patienceGray(
		200,
		450
	)
	enemySpawns.patienceGray(
		400,
		450
	)
	enemySpawns.patienceGray(
		600,
		450
	)
	gunpickups = [];
	newGunPickup(400,250);
	ctx.font = "15px swerveregular";
	var FPS = 60;
	
}
var music = new Audio("redcurare.mp3");

window.onload = function() {
	init();
	tick(0);
}

/*
					db   d8b   db  .d8b.  db    db d88888b .d8888.
					88   I8I   88 d8' `8b 88    88 88'     88'  YP
					88   I8I   88 88ooo88 Y8    8P 88ooooo `8bo.
					Y8   I8I   88 88~~~88 `8b  d8' 88~~~~~   `Y8b.
					`8b d8'8b d8' 88   88  `8bd8'  88.     db   8D
					 `8b8' `8d8'  YP   YP    YP    Y88888P `8888Y'
*/

newWaves = [
	{
		enemy: "bigred",
		startAt: 1,
		basePerWave: 10,
		plusness: 0.25
	},
	{
		enemy: "blackGoon",
		startAt: 1,
		basePerWave: 5,
		plusness: 0.25
	},
	{
		enemy: "spinnyBlue",
		startAt: 2,
		basePerWave: 5,
		plusness: 0.25
	},
	{
		enemy: "yellowBastard",
		startAt: 3,
		basePerWave: 3,
		plusness: 1
	},
	{
		enemy: "orangeKamekaze",
		startAt: 4,
		basePerWave: 3,
		plusness: 1
	},
	{
		enemy: "tealTear",
		startAt: 5,
		basePerWave: 3,
		plusness: 1
	}
]
var clickdir = 0;
var wave;
var waveNumber = 0;
var timePerWave = 20;
var thisWave = 0;
var timePerSpawn;
function prepareWave() {
	thisWave = 0;
	waveNumber++;
	wave = [];
	for (var i = 0; i < newWaves.length; i++) {
		if (newWaves[i].startAt<=waveNumber){
			for (var j = 0; j < newWaves[i].basePerWave+newWaves[i].plusness*(waveNumber-newWaves[i].startAt); j++) {
				wave.push(newWaves[i].enemy);
			}
		}
	}
	timePerSpawn = timePerWave/wave.length;
}

/*
		.d8888. d8888b.  .d8b.  db   d8b   db d8b   db d88888b d8b   db d88888b .88b  d88. db    db       dD Cb
		88'  YP 88  `8D d8' `8b 88   I8I   88 888o  88 88'     888o  88 88'     88'YbdP`88 `8b  d8'     d8'   `8b
		`8bo.   88oodD' 88ooo88 88   I8I   88 88V8o 88 88ooooo 88V8o 88 88ooooo 88  88  88  `8bd8'     d8       8b
		  `Y8b. 88~~~   88~~~88 Y8   I8I   88 88 V8o88 88~~~~~ 88 V8o88 88~~~~~ 88  88  88    88      C88       88D
		db   8D 88      88   88 `8b d8'8b d8' 88  V888 88.     88  V888 88.     88  88  88    88       V8       8P
		`8888Y' 88      YP   YP  `8b8' `8d8'  VP   V8P Y88888P VP   V8P Y88888P YP  YP  YP    YP        V8.   .8P
		                                                                                                  VD CP
*/

function spawnEnemy() {
	spawnIn = timePerSpawn;
	var ind = Math.floor(Math.random()*wave.length);
	var spawn = wave.splice(ind,1);
	var sx = player.x;
	var sy = player.y;
	var dx = player.x - sx;
	var dy = player.y - sy;
	var d = Math.sqrt(dx*dx+dy*dy);
	while (d<250) {
		sx = Math.random()*canvas.width;
		sy = Math.random()*canvas.height;
		dx = player.x - sx;
		dy = player.y - sy;
		d = Math.sqrt(dx*dx+dy*dy);
	}
	enemySpawns[spawn](sx,sy);
}

var tunnelTime = Math.random()*100;
var tunnelAngle = Math.random()*100;
var tunnelRot = 0;
var tunnelBent = Math.random()*100;

var lastTick = +new Date();
var wasMouseDown = false;

/*
			d888888b d888888b  .o88b. db   dD       dD Cb
			`~~88~~'   `88'   d8P  Y8 88 ,8P'     d8'   `8b
			   88       88    8P      88,8P      d8       8b
			   88       88    8b      88`8b     C88       88D
			   88      .88.   Y8b  d8 88 `88.    V8       8P
			   YP    Y888888P  `Y88P' YP   YD     V8.   .8P
			                                        VD CP
*/

function tick() {
	lastdrawcount = drawcount;
	drawcount = 0;
	mouseClicked = mouseDown && !wasMouseDown;
	wasMouseDown = mouseDown;
	var thisTick = +new Date();
	//prec = Math.max(0.01,shake+qshake)*100;
	if (document.hasFocus()){
		var dt = Math.min(1/30,(thisTick-lastTick)/1000);
		update(dt);
		draw(dt);
		window.requestAnimationFrame(tick)
	} else {
		ctx.resetTransform();
		ctx.clearRect(-100,-100,canvas.width+200,canvas.height+200);
		ctx.beginPath();
		ctx.fillStyle = "#333";
		ctx.rect(-100,-100,canvas.width+200,canvas.height+200);
		ctx.fill();
		drawcount++;
		UI({
			y: canvas.height/2,
			textcolor: rgba(255,255,255,0.8),
			title: "PAUSED",
			subtitle:"click to focus",
			now: true,
			lines: []
		});
		setTimeout(tick, 250);
	}
	lastTick = thisTick;
}
var restart = false;
var tunnelRate = 0;
var hovermode = 0;
var keepShooting = 0.5;
var dogif = false;
var shaketime = 0;


/*
			db    db d8888b. d8888b.  .d8b.  d888888b d88888b       dD d8888b. d888888b Cb
			88    88 88  `8D 88  `8D d8' `8b `~~88~~' 88'         d8'  88  `8D `~~88~~'  `8b
			88    88 88oodD' 88   88 88ooo88    88    88ooooo    d8    88   88    88       8b
			88    88 88~~~   88   88 88~~~88    88    88~~~~~   C88    88   88    88       88D
			88b  d88 88      88  .8D 88   88    88    88.        V8    88  .8D    88       8P
			~Y8888P' 88      Y8888D' YP   YP    YP    Y88888P     V8.  Y8888D'    YP     .8P
			                                                        VD                  CP
*/

function update(dt) {
	dt = dt*access.timescale;
	if (gpstate.firing){
		if (!gpstate.lastfired){
			gpstate.lastfired = true;
			gpstate.fired = true;
		} else {
			gpstate.fired = false;
		}
	} else {
		gpstate.lastfired = false;
		gpstate.fired = false;
	}
	global_dt = dt;
	dimension = [window.innerWidth, window.innerHeight];
	
	var dim = Math.min((dimension[0]), (400/300)*(dimension[1]))
	gameElem.style.transform = "scale("+(dim/800)+")";
	
	//dcanvas.width = dim;
	//dcanvas.height = (300/400)*dim;
	
	
	shaketime += dt;
	hovermode = hover.m;
	//dt = dt * 0.25;
	pollGamepads();
	tunnelRate = lerp(tunnelRate,1,dt*2)
	tunnelTime -= dt * tunnelRate;
	tunnelRot += (Math.sin(Math.sin(time)+Math.cos(time*2)/2) + Math.cos(Math.cos(time*0.453)+Math.sin(time*1.235)/2))*dt*0.1;
	tunnelAngle += Math.sin(tunnelRot)*dt* tunnelRate;
	tunnelBent += Math.sin(tunnelAngle)*dt*Math.cos(tunnelRate+tunnelTime);
	if (pulse.active){
		pulse.r += dt*pulse.speed;
		music.volume = Math.pow(Math.min(pulse.r/2000,1),1);
		setGlobalVolume(Math.pow(Math.min(pulse.r/2000,1),1));
		for (var i=0; i<enemies.length; i++){
			var enemy = enemies[i];
			var dx = pulse.x-enemy.x;
			var dy = pulse.y-enemy.y;
			var d = Math.sqrt(dx*dx+dy*dy);
			if (pulse.r<d+enemy.size*3 && pulse.r>d-enemy.size*3){
				enemy.hitby = "pulse";
				enemy.hp = 0;
			}
		}
	}
	recoilx = lerp(recoilx,0,10*dt);
	recoily = lerp(recoily,0,10*dt);
	shake = Math.min(shake-dt*shake*10,1);
	qshake = Math.min(qshake-dt*qshake*30,1);
	if (gstate == states.game){
		if (!over){
			if (started) {
				keepShooting-=dt;
				time += dt;
				thisWave+=dt;
				spawnIn-=dt*1.5;
				if (spawnIn<0){
					if (wave.length<=0){
						prepareWave();
					}
					spawnEnemy();
				}
			} else if (enemies.length==0) {
				if (settings.sound)
					music.play();
				sound.pulse.play()
				prepareWave();
				spawnIn = 2;
				started = true;
				pulse.active = true;
				pulse.r = 0;
				pulse.x = 400;
				pulse.y = 300;
			}
			if (player.current!="null" && keepShooting>0){
				saveImage = true;
				dogif = true;
			} else if (dogif) {
				
				// if (__GIF__)
				// 	gif.render();
				
				dogif = false
			}
		} else {
			if (enemies.length==0 && pulse.r>2400){
				endGame();
			}
		}
		updatePickups(dt);
		updateParticles(dt);
		updateEnemies(dt);
		updateSpaceship(player,dt);
		updateBullets(dt);
		updateOrbits(dt);
	} else {
		if (gpstate.fired && gstate==states.splash){
			startPlaying();
		} else if (gpstate.fired && gstate==states.dead){
			gstate = states.splash;
		}
	}
	var ppdx = canvas.width/2-player.x;
	var ppdy = canvas.height-player.y;
	var ppd2 = (ppdx*ppdx+ppdy*ppdy);
	bui_pos = lerp(bui_pos, ppd2<(200*200)?1:0, 10*dt);
}

function startPlaying() {
	init();
	//splashElements.style.display = "none";
	gstate = states.game;
}
var cgundisp = "";
var bui_pos = 1;
var not_imp = 0;

function wepColor() {
	return effects.weaponRainbow ? hsl(shaketime*600, 1, 0.5) : "white";
}

function notImplemented() {not_imp = 1; shake = 1; qshake =1;}

/*
			d8888b. d8888b.  .d8b.  db   d8b   db       dD d8888b. d888888b Cb
			88  `8D 88  `8D d8' `8b 88   I8I   88     d8'  88  `8D `~~88~~'  `8b
			88   88 88oobY' 88ooo88 88   I8I   88    d8    88   88    88       8b
			88   88 88`8b   88~~~88 Y8   I8I   88   C88    88   88    88       88D
			88  .8D 88 `88. 88   88 `8b d8'8b d8'    V8    88  .8D    88       8P
			Y8888D' 88   YD YP   YP  `8b8' `8d8'      V8.  Y8888D'    YP     .8P
			                                            VD                  CP
*/

function draw(dt) {
	gameElem.classList[score<10 ? 'remove' : 'add']("won");
	ctx.font = "15px Tahoma";
	ctx.resetTransform();
	//ctx.clearRect(-100,-100,canvas.width+200,canvas.height+200);
	ctx.fillStyle = score<10 ? "#244" : "#442";
	ctx.rect(-100,-100,canvas.width+200,canvas.height+200);
	//ctx.fill()
	//ctx.restore();

	ctx.translate(
		Math.sin(shaketime*72)*shake*10*effects.shakeAmount,
		Math.sin(shaketime*48)*shake*10*effects.shakeAmount
	)
	ctx.translate(
		Math.sin(shaketime*48)*qshake*10*effects.shakeAmount,
		Math.sin(shaketime*72)*qshake*10*effects.shakeAmount
	)

	var lw = 200;
	ctx.lineWidth = lw;
	var bdc = 50;
	var blc = 100;
	ctx.fillStyle = score<10 ? rgb(bdc,blc,blc) : rgb(blc,blc,bdc);
	ctx.rect(-100,-100,canvas.width+200,canvas.height+200);
	ctx.fill()
	drawcount++;
	if (effects.psychoTunnel){
		var tSteps = 15;
		ctx.lineWidth = lw;
		var fadeback = 1.4;
		var fadefront = 0.9;
		var dc = bdc*fadeback;
		var lc = blc*fadeback;
		var fdc = bdc*fadefront;
		var flc = blc*fadefront;
		var squish = Math.sin(tunnelTime*0.345);
		for (var i = 2; i<tSteps+2; i++){
			var ti = (i+((-tunnelTime)%2));
			var z = 1-(ti/tSteps);
			z = Math.pow(z,0.5);
			var mdc = lerp(fdc,dc,z*z);
			var mlc = lerp(flc,lc,z*z);
			if (i%2==0){
			} else {
				mdc = mdc*0.9;
				mlc = mlc*0.9;
			}
			ctx.strokeStyle = score<10 ? rgb(mdc,mlc,mlc) : rgb(mlc,mlc,mdc);
			var a = (Math.cos(squish)+squish)-z;
			var d = 500*z*z*z*-Math.cos(z*Math.PI)*1.5;
			var dx = Math.cos(a)*d;
			var dy = Math.sin(a)*d;
			var rad = 1100;
			var zmult = (1-Math.pow(z,0.75))-lw/rad/2
			if (zmult>-lw)
				linecircle(400+dx,300+dy,rad*zmult,5,tunnelAngle-Math.cos(tunnelTime-z)*0.5)
		}
	}
	var killsleft = 15 - sinceLast;
	var highlightness = Math.max(0,Math.min(1,flashPick))
	highlightness = Math.pow(highlightness,2)
	var c = 255*highlightness
	var highcolor = rgba(c,c,c,Math.max(0.5,highlightness));
	var highlightness2 = Math.max(0,Math.min(1,flashWep))
	highlightness2 = Math.pow(highlightness2,2)
	var c2 = 255*highlightness2;
	var highcolor2 = rgba(c2,c2,c2,Math.max(0.5,highlightness2));
	var highlightness3 = Math.max(0,Math.min(1,flashSpawn))
	highlightness3 = Math.pow(highlightness3,2)
	var c3 = 255*highlightness3;
	var highcolor3 = rgba(c3,c3,c3,Math.max(0.5,highlightness3));
	var gameUI = {
		y: canvas.height/2,
		title: cgundisp,
		titlecolor: highcolor2,
		subtitle: gunpickups.length<=0 ? "next in: "+killsleft : "weapon ready",
		subcolor: highcolor3,
		lines: [
			{
				left: "time: "+Math.floor(time*100)/100,
				center: gunpickups.length<=0 ? "eliminate targets" : "collect weapon",
				ccolor: gunpickups.length<=0 ? highcolor : wepColor(),
				right: "score: "+score+"/10",
				rcolor: highcolor2
			},
			{
				left: "fps",
				center: "",
				right: "" + Math.floor((1/dt)*100)/100
			},
			{
				left: "enemies",
				center: "",
				right: "" + enemies.length
			},
			{
				left: "bullets",
				center: "",
				right: "" + bullets.length
			},
			{
				left: "drawcount",
				center: "",
				right: "" + lastdrawcount
			}
			
		]
	};
	var usefuls = 0;
  for (var kill in stats.kills) {
    if (stats.kills.hasOwnProperty(kill)) {
      var k=stats.kills[kill]
      if (k.u)
        usefuls++;
    }
  }

  var eff = usefuls/stats.kills.length;
  eff = Math.floor(eff*100);
	var deadUI = {
		y: 200,
		title: "GAME OVER",
		subtitle: "",
		lines: [
			{
				left: "score",
				center:"",
				ccolor: rgba(0,255,127,0.8),
				right: score+"/10",
				rcolor: rgba(255,255,255,0.8)
			},
			{
					left:"",
					center:"",
					ccolor: rgba(0,255,127,0.8),
					right:""
			},
			{
					left:"survived",
					center:"",
					ccolor: rgba(0,255,127,0.8),
					right: stats.get("death",{}).get("t",0)+"s"
			},
			{
					left:"eliminations",
					center:"",
					ccolor: rgba(0,255,127,0.8),
					right: stats.get("kills",[]).length
			},
			{
					left:"efficiency",
					center:"",
					ccolor: rgba(0,255,127,0.8),
					right: eff+"%"
			},
			{
					left:"",
					center:"",
					ccolor: rgba(0,255,127,0.8),
					right:""
			},
			{
					left:"back to menu",
					lfunc: function(){gstate = states.splash},
					center:"play again",
					cfunc: startPlaying,
					right:"high-scores",
					rfunc: notImplemented
			}
		]
	}
	var splashUI = {
		textcolor: rgba(0,0,0,0.75),
		y: 200,
		titlecolor: rgba(0,0,0,0.35),
		title: gameName,
		subtitle: "v0.1",
		//defaultfunc: startPlaying,
		lines: [
			{
				left: "",
				center: "",
				right: "nanoleptic(gaeel)"
			},
			{
				left: "",
				center: "",
				right: ""
			},
			{
				left: "",
				center: "click to start",
				cfunc: startPlaying,
				right: "",
				ccolor: rgba(255,255,255,0.5)
			},
			{
				left: "",
				center: "(or press A on your controller)",
				right: "",
				ccolor: rgba(255,255,255,0.5)
			},
			{
				left: "",
				center: "options",
				cfunc: function(){
					gstate = states.options;
				},
				right: ""
			},
			{
				left: "",
				center: "scores",
				cfunc: notImplemented,
				right: ""
			},
			{
				left: "",
				center: "",
				right: ""
			},
			{
				lcolor: rgba(0,0,0,0.55),
				left: "design & code:",
				center: "",
				right: "Kevin \"Gaeel\" Bradshaw",
				rfunc: clickFunk("https://nanoleptic.net/gaeel"),
				rcolor: rgba(255,255,255,0.5)
			},
			{
				left: "",
				center: "",
				right: ""
			},
			{
				lcolor: rgba(0,0,0,0.55),
				left: "music:",
				center: "",
				right: "Red Curare - Cyanide Dansen",
				rfunc: clickFunk("https://cyanidedansen.bandcamp.com/track/red-curare"),
				rcolor: rgba(255,255,255,0.5)
			},
			{
				left: "",
				center: "",
				right: "Zenzoo Pop EP available NOW!",
				rcolor: hsl(Math.sin(shaketime*3)*30+30, 0.5, 0.5),
				rfunc: clickFunk("https://cyanidedansen.bandcamp.com/album/zenzoo-pop")
			}
		]
	};
	if (not_imp>0){
		not_imp-=global_dt;
		UI({
			y:canvas.height/2,
			now: true,
			title: "ERROR",
			titlecolor: rgba(180,20,20,0.5),
			subtitle: "",
			lines: [
				{
					left:"",
					center: "",
					right:"not implemented yet"
				}
			]
		});
	} else {
		switch (gstate) {
			case states.options:
				UI(GenerateOptionUIs());
				break;
			case states.splash:
				UI(splashUI);
				break;
			case states.menu:
				UI(menuUI);
				break;
			case states.game:
				UI(gameUI);
				break;
			case states.dead:
				UI(deadUI);
				break;
		}
	}
	/*
	for (var i = 0; i < stars.length; i++) {
		var star = stars[i];
		ctx.fillStyle = "white";
		ctx.fillRect(
			star.x-lerp(player.x/5,0,i/stars.length),
			star.y-lerp(player.y/5,0,i/stars.length),
			1,1);
	}
	*/
	if (gstate==states.game){
		if (Math.random()>0.65){
			cgundisp = jumbleTo(cgundisp, player.current.toUpperCase());
		} else if (Math.random()*0.5<shake) {
			var err = "";
			switch (irand(1,3)) {
				case 1:
					var reps = cgundisp.length;//+irand(-2,2);
					reps = Math.max(reps, 0);
					err = "#".repeat(reps);
					break;
				case 2:
					err = cgundisp.toLowerCase();
					break;
				case 3:
					err = weaponNames[irand(0,weaponNames.length-1)];
					
			}
			cgundisp = jumbleTo(cgundisp, err);
		}
		
		shipBack(player);
		drawParticles();
		drawBulletsBack();
		drawBulletsFront();
		drawOrbits();
		drawEnemies();
		drawPickups();
		drawSpaceship(player);
		if (pulse.active){
			ctx.strokeStyle = "white";
			ctx.lineWidth = 20;
			for (var rmin = 0; rmin<=400; rmin+=100){
				if (pulse.r>rmin)
					linecircle(pulse.x,pulse.y,pulse.r-rmin,7,time);
			}
			ctx.strokeStyle = "white";
			ctx.lineWidth = 1;
		}
	}
	if (gstate == states.game && !gpstate.active) {
		var mx = access.autoaim ? aatarget.x : mouseX;
		var my = access.autoaim ? aatarget.y : mouseY;
		ctx.strokeStyle = "white";
		ctx.lineWidth = lerp(1+Math.sin(shaketime*20),0,hovermode);
		linecircle(mx,my,lerp(14+Math.sin(time*30)*1,0,hovermode),7,shaketime);
		if (!hovering){
			ctx.fillStyle = "white";
			circle(mx,my,lerp(8,0,hovermode),7,shaketime);
		}
	} else if (!gpstate.active){
		ctx.strokeStyle = "white";
		ctx.lineWidth = lerp(1+Math.sin(shaketime*20),0,hovermode);
		linecircle(mouseX,mouseY,lerp(14+Math.sin(time*30)*1,0,hovermode),7,shaketime);
		if (!hovering){
			ctx.fillStyle = "white";
			circle(mouseX,mouseY,lerp(8,0,hovermode),7,shaketime);
		}
	} else {
		ctx.strokeStyle = "white";
		ctx.fillStyle = "white";
		ctx.lineWidth = 6;
		var xoff = Math.cos(gpstate.aim);
		var yoff = Math.sin(gpstate.aim);
		var cd = 15
		ctx.beginPath();
		ctx.moveTo(
			player.x+xoff*25-yoff*cd/4,
			player.y+yoff*25+xoff*cd/4
		)
		ctx.lineTo(
			player.x+xoff*15-yoff*cd,
			player.y+yoff*15+xoff*cd
		)
		ctx.lineTo(
			player.x+xoff*20-yoff*cd/2,
			player.y+yoff*20+xoff*cd/2
		)
		ctx.lineTo(
			player.x+xoff*20+yoff*cd/2,
			player.y+yoff*20-xoff*cd/2
		)
		ctx.lineTo(
			player.x+xoff*15+yoff*cd,
			player.y+yoff*15-xoff*cd
		)
		ctx.lineTo(
			player.x+xoff*25+yoff*cd/4,
			player.y+yoff*25-xoff*cd/4
		)
		ctx.closePath()
		ctx.fill()
		ctx.stroke()
		drawcount++;
		drawcount++;
		
	}

	/*
	ctx.fillStyle = "red";
	ctx.font = "60px Tahoma";
	var t = "BREAK TIME (eating food)";
	ctx.fillText(t,canvas.width/2-ctx.measureText(t).width/2,canvas.height/2+15);
	//*/

	if (saveImage){
		saveImage = false;

		if (__DEBUG__){
			// save canvas image as data url (png format by default)
			//var dataURL = canvas.toDataURL();

			// set canvasImg image src to dataURL
			// so it can be saved as an image
			//var im = document.createElement('img');
			//im.src = dataURL;
			//document.getElementById('scrots').appendChild(im);
			if (__GIF__)
				gif.addFrame(ctx, {copy: true, delay: 16});
		}
	}
	/*
	if (selected!=null){
		var thing = selected.target.parentElement;
		ctx.rect(thing.offsetLeft,thing.offsetTop,thing.offsetWidth,thing.offsetHeight);
		ctx.lineWidth = 2;
		ctx.stroke();
	}
	*/


	//var s = Math.min(dcanvas.width/800, dcanvas.height/600);
	//var dw = 800
	//var dh = 600
	//var w = dcanvas.width;
	//var h = dcanvas.height;
  //dctx.clearRect(0,0,dcanvas.width,dcanvas.height);
	//dctx.resetTransform();
	//dctx.scale(s,s);
  //dctx.drawImage(canvas,(w/s - dw)/2,(h/s - dh)/2);
}

var prec = 0.01;

/*
	 d888b  d8888b.  .d8b.  d8888b. db   db d888888b  .o88b. .d8888.   db    db d888888b d888888b db      .d8888.
	88' Y8b 88  `8D d8' `8b 88  `8D 88   88   `88'   d8P  Y8 88'  YP   88    88 `~~88~~'   `88'   88      88'  YP
	88      88oobY' 88ooo88 88oodD' 88ooo88    88    8P      `8bo.     88    88    88       88    88      `8bo.
	88  ooo 88`8b   88~~~88 88~~~   88~~~88    88    8b        `Y8b.   88    88    88       88    88        `Y8b.
	88. ~8~ 88 `88. 88   88 88      88   88   .88.   Y8b  d8 db   8D   88b  d88    88      .88.   88booo. db   8D
	 Y888P  88   YD YP   YP 88      YP   YP Y888888P  `Y88P' `8888Y'   ~Y8888P'    YP    Y888888P Y88888P `8888Y'
*/

function distort2(xy) {
	var dx = xy.x-400;
	var dy = xy.y-300;
	var d = Math.sqrt(dx*dx+dy*dy);
	var a = Math.atan2(dy,dx);
	//a = a+Math.sin(d/100)*(shake+qshake);
	d = d/350;
	d = Math.pow(d,1-shake);
	d = d*350;
	return {
		x:d*Math.cos(a)+400,
		y:d*Math.sin(a)+300,
	}
}

function distort(x, f) {
	x=x-f;
	x=x/f;
	x=Math.abs(Math.pow(Math.abs(x),1.5+Math.sin(time)))*Math.sign(x);
	x=x*f;
	x=x+f;
	return x;
}

function myMoveTo(x, y) {
	var xy = {x:x,y:y};
	//xy = distort2(xy)
	x=xy.x;
	y=xy.y;
	ctx.moveTo(
		Math.floor(x/prec)*prec,
		Math.floor(y/prec)*prec
	)
}

function myLineTo(x, y) {
	var xy = {x:x,y:y};
	//xy = distort2(xy)
	x=xy.x;
	y=xy.y;
	ctx.lineTo(
		Math.floor(x/prec)*prec,
		Math.floor(y/prec)*prec
	)
}

// Graphical helpers
function circle(x,y,r,segments,startAngle) {
	var segs = segments ? segments : 5;
	ctx.beginPath();

	var currentAngle = startAngle ? (effects.lockCircles ? 0 : startAngle) : 0;
	myMoveTo(
		x + Math.cos(currentAngle)*r,
		y + Math.sin(currentAngle)*r
	);
	for (var i=1; i<segs; i++){
		currentAngle += (Math.PI*2)/segs;
		myLineTo(
			x + Math.cos(currentAngle)*r,
			y + Math.sin(currentAngle)*r
		);
	}

	ctx.fill();
	drawcount++;
}
function linecircle(x,y,r,segments,startAngle,cb) {
	var segs = segments ? segments : 5;
	ctx.beginPath();

	var currentAngle = startAngle ? (effects.lockCircles ? 0 : startAngle) : 0;
	myMoveTo(
		x + Math.cos(currentAngle)*r,
		y + Math.sin(currentAngle)*r
	);
	for (var i=1; i<segs+1; i++){
		currentAngle += (Math.PI*2)/segs;
		myLineTo(
			x + Math.cos(currentAngle)*r,
			y + Math.sin(currentAngle)*r
		);
		if (cb){
			cb(
				x + Math.cos(currentAngle)*r,
				y + Math.sin(currentAngle)*r,
				currentAngle
			)
		}
	}
	ctx.closePath();
	ctx.stroke();
	drawcount++;
}

function line(x1,y1,x2,y2) {
	ctx.beginPath();
	myMoveTo(x1,y1);
	myLineTo(x2,y2);
	ctx.stroke();
	drawcount++;
}
function fline(x1,y1,x2,y2) {
	line(
		Math.floor(x1),
		Math.floor(y1),
		Math.floor(x2),
		Math.floor(y2)
	)
}

//Mathematical helpers
function lerp(a,b,n) {
	return n*b+(1-n)*a;
}


/*
		d888888b d8b   db d8888b. db    db d888888b   db    db d888888b d888888b db      .d8888.
		  `88'   888o  88 88  `8D 88    88 `~~88~~'   88    88 `~~88~~'   `88'   88      88'  YP
		   88    88V8o 88 88oodD' 88    88    88      88    88    88       88    88      `8bo.
		   88    88 V8o88 88~~~   88    88    88      88    88    88       88    88        `Y8b.
		  .88.   88  V888 88      88b  d88    88      88b  d88    88      .88.   88booo. db   8D
		Y888888P VP   V8P 88      ~Y8888P'    YP      ~Y8888P'    YP    Y888888P Y88888P `8888Y'
*/

mouseDown = false;
mouseClicked = false;
mouseX = 0;
mouseY = 0;
document.onmousemove = function(mouseEvent){
	//if (!hovering){
	var rect = canvas.getBoundingClientRect();
	//console.log(rect.top, rect.right, rect.bottom, rect.left);
	
	mouseX = (mouseEvent.pageX-rect.left)/(rect.width/800);
	mouseY = (mouseEvent.pageY-rect.top)/(rect.height/600);
	//}
}
document.onmousedown = function(mouseEvent){
	gpstate.active = false;
	mouseDown = true;
	clickdir = 1;
}
var urlonup = "";
document.onmouseup = function(mouseEvent) {
	if (urlonup.length>0){
		openInNewTab(urlonup);
		urlonup = "";
	}
	mouseDown = false;
	clickdir = -1;
}

var keys = [];
document.onkeydown = function(keyEvent) {
	gpstate.active = false;
	keys[keyEvent.keyCode] = true;
	if (gunpickups.length>0){
		var guncurrent = gunpickups[0].weapon;
		if (keyEvent.keyCode==82){
			guncurrent = weaponNames[mod(weaponNames.indexOf(guncurrent)+1,weaponNames.length)];
		}
		if (keyEvent.keyCode==84){
			guncurrent = weaponNames[mod(weaponNames.indexOf(guncurrent)-1,weaponNames.length)];
		}
		gunpickups[0].weapon = guncurrent;
	}
	if (keyEvent.keyCode==89){
		saveImage = true;
	}

}
document.onkeyup = function(keyEvent) {
	keys[keyEvent.keyCode] = false;
}




var gamepads = {};

function gamepadHandler(event, connecting) {
  var gamepad = event.gamepad;
  // Note:
  // gamepad === navigator.getGamepads()[gamepad.index]

  if (connecting) {
    gamepads[gamepad.index] = gamepad;
  } else {
    delete gamepads[gamepad.index];
  }
}

window.addEventListener("gamepadconnected", function(e) { gamepadHandler(e, true); }, false);
window.addEventListener("gamepaddisconnected", function(e) { gamepadHandler(e, false); }, false);

function buttonPressed(b) {
  if (typeof(b) == "object") {
    return b.pressed;
  }
  return b == 1.0;
}

gpstate = {
	active: false,
	firing: false,
	fired:false,
	lastfired: false,
	movement:{
		x: 0,
		y: 0
	},
	aim: 0
}

// Nice deadzoning
//	raw contains the x and y of the joystick
//	dead is the threshold (0.3 for good results)
function deadZoner(raw, dead) {
	var d = raw.x*raw.x + raw.y*raw.y;

	if (d<dead*dead){
		// We're inside the deadzone, return zero
		return {x: 0, y:0};

	} else {
		// d is squarerooted to get actual distance from zero
		d = Math.sqrt(d);

		// Normalise raw input
		var nx = raw.x/d;
		var ny = raw.y/d;

		// then repositioned & rescaled so (dead->1) becomes (0->1)
		// and clamped down so we get 0 <= d <= 1
		d = Math.min((d-dead)/(1-dead),1);

		// Scale normalised input and return
		return {
			x: nx*d,
			y: ny*d
		}
	}
}

function pollGamepads() {
	gpstate.firing = false;
	var gamepads = navigator.getGamepads ? navigator.getGamepads() : (navigator.webkitGetGamepads ? navigator.webkitGetGamepads() : []);
  if (!gamepads) {
    return;
  }
	if (gamepads.length>0){
		var gp = gamepads[0];
		if (gp){
			for (var i = 0; i < gp.buttons.length; i++) {
				if (buttonPressed(gp.buttons[i])) {
					gpstate.active = true;
					gpstate.firing = true;
				}
			}
			gpstate.movement = deadZoner({
				x: gp.axes[0],
				y: gp.axes[1]
			},0.3)
			var aim = deadZoner({
				x: gp.axes[2],
				y: gp.axes[3]
			},0.3)
			var aimd = (aim.x*aim.x+aim.y*aim.y)
			if (aimd>0){
				//gpstate.firing = true
				gpstate.active = true;
				gpstate.aim = Math.atan2(aim.y,aim.x);
			}
		}
	} else {
		gpstate.movement = {x:0,y:0};
		gpstate.aim = 0;
	}
}

function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function toggleFullScreen(){
           if(canvas.webkitRequestFullScreen) {
               canvas.webkitRequestFullScreen();
           }
          else {
             canvas.mozRequestFullScreen();
          }
}
var hovering = false;
var selected = null;
document.addEventListener('mouseover',function(e){
    //var cursor = e.target.style.cursor;
		hovering = e.target.className.indexOf("hover")!=-1;
		selected = e;
},true);

function jumbleTo(from, to) {
	if (from.length > to.length){
		var cut = irand(1,from.length);
		var s = from.substring(0,cut-1)+from.substring(cut);
		return s;
	}
	if (from.length < to.length){
		var ins = irand(0,from.length);
		var fpos = irand(1,to.length);
		var s = from.substring(0,ins)+to.substring(fpos-1,fpos)+from.substring(ins);
	}
	var pots = []
	for (var i = 0; i < to.length; i++) {
		if (to[i]!=from[i]){
			pots.push(i)
		}
	}
	if (pots.length>0){
		var pos = pots[irand(0,pots.length-1)];
		return from.substring(0,pos)+to[pos]+from.substring(pos+1);
	}
	return from;
}

function irand(a,b) {
	var m = Math.floor(Math.min(a,b))
	var M = 1+Math.ceil(Math.max(a,b))
	return m+Math.floor(Math.random()*(M-m));
}

var default_text = {
	size: 16,
	color: "#FFF",
	font: "swerveregular"
}

function rgba(r,g,b,a) {
	return "rgba("+Math.max(0,Math.min(255,Math.floor(r)))+", "+Math.max(0,Math.min(255,Math.floor(g)))+", "+Math.max(0,Math.min(255,Math.floor(b)))+", "+Math.floor(a*100)/100+")";
}
function rgb(r,g,b,a) {
	return "rgb("+Math.max(0,Math.min(255,Math.floor(r)))+", "+Math.max(0,Math.min(255,Math.floor(g)))+", "+Math.max(0,Math.min(255,Math.floor(b)))+")";
}

function hsl(h,s,l) {
	return "hsl("+Math.floor(h%360)+", "+Math.floor(Math.max(0, Math.min(1,s))*100)+"%, "+Math.floor(Math.max(0, Math.min(1,l))*100)+"%)";
}
function hsla(h,s,l,a) {
	return "hsl("+Math.floor(h%360)+", "+Math.floor(Math.max(0, Math.min(1,s))*100)+"%, "+Math.floor(Math.max(0, Math.min(1,l))*100)+"%, "+Math.floor(a*100)/100+")";
}

var uistuff = {
	y: canvas.height,
	title: "",
	subtitle: "",
	lines: []
}
for (var i = 0; i<13; i++){
	uistuff.lines.push({
		left:"",
		center:"",
		right:"",
	})
}
hover = {
	x: 0,
	y: 0,
	w: 0,
	h: 0,
	m: 0
}
hovertarget = {
	x: 0,
	y: 0,
	w: 0,
	h: 0,
	m: 0
}
function UI(newstuff) {
	var now = newstuff.get("now",false);
	uistuff.y = now ? newstuff.y : lerp(uistuff.y, newstuff.y, global_dt*10);
	uistuff.title = now ? ""+newstuff.title : jumbleTo(""+uistuff.title, ""+newstuff.title);
	uistuff.subtitle = now ? ""+newstuff.subtitle : jumbleTo(""+uistuff.subtitle, ""+newstuff.subtitle);
	for (var i = 0; i < Math.min(uistuff.lines.length, newstuff.lines.length); i++) {
		for (var entry in newstuff.lines[i]) {
			if (newstuff.lines[i].hasOwnProperty(entry)) {
				uistuff.lines[i][entry] = newstuff.lines[i][entry];
			}
		}
		for (var entry in uistuff.lines[i]) {
			if (!newstuff.lines[i].hasOwnProperty(entry)) {
				delete uistuff.lines[i][entry];
			}
		}
		if (!now){
			uistuff.lines[i].left = jumbleTo(""+uistuff.lines[i].left, ""+newstuff.lines[i].left);
			uistuff.lines[i].center = jumbleTo(""+uistuff.lines[i].center, ""+newstuff.lines[i].center);
			uistuff.lines[i].right = jumbleTo(""+uistuff.lines[i].right, ""+newstuff.lines[i].right);
		}
	}
	if (uistuff.lines.length>newstuff.lines.length){
		for (var i = newstuff.lines.length; i < uistuff.lines.length; i++) {
			uistuff.lines[i].left = now ? "" : jumbleTo(""+uistuff.lines[i].left, "");
			uistuff.lines[i].center = now ? "" : jumbleTo(""+uistuff.lines[i].center, "");
			uistuff.lines[i].right = now ? "" : jumbleTo(""+uistuff.lines[i].right, "");
		}
	}
	
	var heavyfont = "swerve_bold";
	var lightfont = "EurostileExtended-Roman";
	var textcolor = newstuff.get("textcolor", rgba(0,0,0,0.5));
	var heavysize = 80;
	var lightsize = 15;
	hovertarget.w = 0;
	hovertarget.m = 0;
	hovertarget.x = mouseX;
	hovertarget.y = mouseY-10;
	var func = newstuff.get("defaultfunc", function() {});
	for (var i = 0; i < newstuff.lines.length; i++) {
		var l = newstuff.lines[i];
		if (l.lfunc && mouseOnRightText(uistuff.lines[i].left, 100, uistuff.y+(i+1)*(10+lightsize), lightsize, lightfont)){
			var b = rightBox(uistuff.lines[i].left, 100, uistuff.y+(i+1)*(10+lightsize), lightsize, lightfont);
			hovertarget.x = b.x+b.w/2;
			hovertarget.y = b.y-4;
			hovertarget.w = b.w+6;
			hovertarget.h = b.h;
			hovertarget.m = 1;
			func = l.lfunc;
			break;
		}
		if (l.cfunc && mouseOnCenteredText(uistuff.lines[i].center, canvas.width/2, uistuff.y+(i+1)*(10+lightsize), lightsize, lightfont)){
			var b = centerBox(uistuff.lines[i].center, canvas.width/2, uistuff.y+(i+1)*(10+lightsize), lightsize, lightfont);
			hovertarget.x = b.x+b.w/2;
			hovertarget.y = b.y-4;
			hovertarget.w = b.w+6;
			hovertarget.h = b.h;
			hovertarget.m = 1;
			func = l.cfunc;
			break;
		}
		if (l.rfunc && mouseOnLeftText(uistuff.lines[i].right, canvas.width-100, uistuff.y+(i+1)*(10+lightsize), lightsize, lightfont)){
			var b = leftBox(uistuff.lines[i].right, canvas.width-100, uistuff.y+(i+1)*(10+lightsize), lightsize, lightfont);
			hovertarget.x = b.x+b.w/2;
			hovertarget.y = b.y-4;
			hovertarget.w = b.w+6;
			hovertarget.h = b.h;
			hovertarget.m = 1;
			func = l.rfunc;
			break;
		}
	}
	hover.x = lerp(hover.x, hovertarget.x, 40*global_dt);
	hover.y = lerp(hover.y, hovertarget.y, 40*global_dt);
	hover.w = lerp(hover.w, hovertarget.w, 10*global_dt);
	hover.h = hovertarget.h;
	hover.m = lerp(hover.m, hovertarget.m, 20*global_dt);
	ctx.fillStyle = rgba(255,255,255,0.25);
	rectangleC(hover.x, hover.y, hover.w, hover.h);
	ctx.fill();
	drawcount++;
	
	if (mouseClicked){
		func();
	}
	
	ctx.strokeStyle = textcolor;
	ctx.lineWidth = 2;
	line(100, uistuff.y,canvas.width-100,uistuff.y);
	
	drawTextRight(uistuff.subtitle, canvas.width-100, uistuff.y-10, lightsize, newstuff.get("subcolor", textcolor), lightfont);
	drawTextLeft(uistuff.title, 100, uistuff.y-10, heavysize, newstuff.get("titlecolor", textcolor), heavyfont);
	for (var i = 0; i < uistuff.lines.length; i++) {
		var lfunked = i<newstuff.lines.length && newstuff.lines[i].lfunc != undefined;
		drawTextLeft(uistuff.lines[i].left, 100, uistuff.y+(i+1)*(10+lightsize), lightsize, uistuff.lines[i].get("lcolor", textcolor), lightfont, lfunked);
		var cfunked = i<newstuff.lines.length && newstuff.lines[i].cfunc != undefined;
		drawTextCentered(uistuff.lines[i].center, canvas.width/2, uistuff.y+(i+1)*(10+lightsize), lightsize, uistuff.lines[i].get("ccolor", textcolor), lightfont, cfunked);
		var rfunked = i<newstuff.lines.length && newstuff.lines[i].rfunc != undefined;
		drawTextRight(uistuff.lines[i].right, canvas.width-100, uistuff.y+(i+1)*(10+lightsize), lightsize, uistuff.lines[i].get("rcolor", textcolor), lightfont, rfunked);
	}
	
	ctx.font = "18px swerveregular";
}

function mouseOnCenteredText(text, x, y, size, font){
	ctx.font = size+"pt "+(font!=undefined ? font : "swerve_bold");
	var w = ctx.measureText(text).width;
	return (mouseX>=x-w/2 && mouseX<=x+w/2 && mouseY>=y-2-size && mouseY<=y+4);
}
function centerBox(text, x, y, size, font){
	ctx.font = size+"pt "+(font!=undefined ? font : "swerve_bold");
	var w = ctx.measureText(text).width;
	return {
		x: x-w/2,
		y: y-size,
		w: w,
		h: size*2
	}
}
function mouseOnLeftText(text, x, y, size, font){
	ctx.font = size+"pt "+(font!=undefined ? font : "swerve_bold");
	var w = ctx.measureText(text).width;
	return (mouseX>=x-w && mouseX<=x && mouseY>=y-2-size && mouseY<=y+4);
}
function leftBox(text, x, y, size, font){
	ctx.font = size+"pt "+(font!=undefined ? font : "swerve_bold");
	var w = ctx.measureText(text).width;
	return {
		x: x-w,
		y: y-size,
		w: w,
		h: size*2
	}
}
function mouseOnRightText(text, x, y, size, font){
	ctx.font = size+"pt "+(font!=undefined ? font : "swerve_bold");
	var w = ctx.measureText(text).width;
	return (mouseX>=x && mouseX<=x+w && mouseY>=y-2-size && mouseY<=y+4);
}
function rightBox(text, x, y, size, font){
	ctx.font = size+"pt "+(font!=undefined ? font : "swerve_bold");
	var w = ctx.measureText(text).width;
	return {
		x: x,
		y: y-size,
		w: w,
		h: size*2
	}
}

function drawTextCentered(text, x, y, size, color, font, underlined) {
	ctx.fillStyle = color;
	ctx.font = size+"pt "+(font!=undefined ? font : "swerve_bold");
	var w = ctx.measureText(text).width;
	ctx.fillText(text,x-w/2,y);
	drawcount++;
	if (underlined){
		ctx.beginPath();
	  ctx.strokeStyle = color;
	  ctx.lineWidth = 1.5;
	  ctx.moveTo(x-w/2+size/2,y+3);
	  ctx.lineTo(x+w/2-size/2,y+3);
	  ctx.stroke();
		drawcount++;
	}
}
function drawTextLeft(text, x, y, size, color, font, underlined) {
	ctx.fillStyle = color;
	ctx.font = size+"pt "+(font!=undefined ? font : "swerve_bold");
	ctx.fillText(text,x,y);
	drawcount++;
	var w = ctx.measureText(text).width;
	if (underlined){
		ctx.beginPath();
	  ctx.strokeStyle = color;
	  ctx.lineWidth = 1.5;
	  ctx.moveTo(x+size/2,y+3);
	  ctx.lineTo(x+w-size/2,y+3);
	  ctx.stroke();
		drawcount++;
	}
}
function drawTextRight(text, x, y, size, color, font, underlined) {
	ctx.fillStyle = color;
	ctx.font = size+"pt "+(font!=undefined ? font : "swerve_bold");
	var w = ctx.measureText(text).width;
	ctx.fillText(text,x-w,y);
	drawcount++;
	if (underlined){
		ctx.beginPath();
	  ctx.strokeStyle = color;
	  ctx.lineWidth = 1.5;
	  ctx.moveTo(x-w+size/2,y+3);
	  ctx.lineTo(x-size/2,y+3);
	  ctx.stroke();
		drawcount++;
	}
}
Object.prototype.get = function(str, def){
  return this.hasOwnProperty(str) ? this[str] : def;
}

function rectangle(x,y,w,h) {
	ctx.beginPath();
	ctx.moveTo(x, y);
	ctx.lineTo(x+w, y);
	ctx.lineTo(x+w, y+h);
	ctx.lineTo(x, y+h);
	ctx.closePath();
	drawcount++;
}
function rectangleC(x,y,w,h) {
	ctx.beginPath();
	ctx.moveTo(x-w/2, y);
	ctx.lineTo(x+w/2, y);
	ctx.lineTo(x+w/2, y+h);
	ctx.lineTo(x-w/2, y+h);
	ctx.closePath();
	drawcount++;
}

function createCookie(name,value,days) {
	var expires = "";
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days*24*60*60*1000));
		expires = "; expires=" + date.toUTCString();
	}
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}

function clickFunk(url) {
	return function(){
		urlonup = url;
	}
}

function openInNewTab(url) {
  var win = window.open(url, '_blank');
  window.focus();
}

