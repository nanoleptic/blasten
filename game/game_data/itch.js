document.addEventListener('DOMContentLoaded', function () {
  if (typeof Itch !== 'undefined') {
    var xhr = new XMLHttpRequest()
    xhr.onreadystatechange = function () {
      if (xhr.readyState !== 4) {
        return
      }

      const res = JSON.parse(xhr.responseText)
      if (res.errors) {
        alert("Error getting itch stuff");
      } else {
        var itchstuff = "";
        itchstuff += ('Authenticated as ' + (res.user.display_name || res.user.username) + ' ')
        itchstuff += ('\n' + JSON.stringify(res.user, 0, 2))
        alert(itchstuff)
      }
    }
    xhr.open('GET', 'https://itch.io/api/1/jwt/me')
    xhr.setRequestHeader('Authorization', Itch.env.ITCHIO_API_KEY)
    xhr.send()
  } else {
    alert("No integration")
  }
})