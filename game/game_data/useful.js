function circle(x,y,r) {
	ctx.beginPath();
	ctx.arc(x,y,r,0,Math.PI*2);
	ctx.fill();
}
function mod(x,m) {
	return ((x%m)+m)%m;
}

function fromto(x1,y1,x2,y2,md) {
	var dx = x2-x1;
	var dy = y2-y1;
	var d2 = dx*dx+dy*dy;
	if (d2<md*md){
		return [x2,y2];
	} else {
		var d = Math.sqrt(d2);
		return [x1+(dx/d)*md, y1+(dy/d)*md];
	}
}