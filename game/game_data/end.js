function endGame(){
  console.log("Woop!");
  //scoreElements.style.display = "block";
  gstate = states.dead;
  bullets.map(purgify);
  enemies.map(purgify);
  particles.map(purgify);
  gunpickups = [];
  updateParticles(0.1);
  updateEnemies(0.1);
  updateBullets(0.1);
  if (!sentScore){
    quickScore();
    sentScore = true;
  }
  gunpickups = [];
  sinceLast = 0;
  player.current = "null";
}

//claimScoreElements.submit = claimScore;
var claim;
function claimScore(e) {
    var nick = document.getElementById("nickname");
    console.log("Claimed as "+nick.value);
    claim = {
      game: stats.game,
      nick: nick.value
    };

    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance
    xmlhttp.open("POST", "https://brrrp-conek.rhcloud.com/claim");
    //xmlhttp.open("POST", "http://127.0.0.1/stats");
    xmlhttp.setRequestHeader("Content-Type", "text/plain;charset=UTF-8");
    xmlhttp.send(JSON.stringify(claim));
}

function purgify(t) {
  t.purge = true;
}

function quickScore() {
  var usefuls = 0;
  for (var kill in stats.kills) {
    if (stats.kills.hasOwnProperty(kill)) {
      var k=stats.kills[kill]
      if (k.u)
        usefuls++;
    }
  }

  var eff = usefuls/stats.kills.length;
  eff = Math.floor(eff*100);
  var scorecard = {
    
    
  }
}


function showScores(){
    var theUrl = "https://brrrp-conek.rhcloud.com/scores";

    var killTypes = {};

    var xhr = createCORSRequest('GET', theUrl);
    if (!xhr) {
      throw new Error('CORS not supported');
    }

    xhr.onload = function() {
     var responseText = xhr.responseText;
     // process the response.
    };

    xhr.onerror = function() {
      console.log('There was an error retrieving stat data...');
    };

    xhr.send();
}



function createCORSRequest(method, url) {
  var xhr = new XMLHttpRequest();
  if ("withCredentials" in xhr) {

    // Check if the XMLHttpRequest object has a "withCredentials" property.
    // "withCredentials" only exists on XMLHTTPRequest2 objects.
    xhr.open(method, url, true);

  } else if (typeof XDomainRequest != "undefined") {

    // Otherwise, check if XDomainRequest.
    // XDomainRequest only exists in IE, and is IE's way of making CORS requests.
    xhr = new XDomainRequest();
    xhr.open(method, url);

  } else {

    // Otherwise, CORS is not supported by the browser.
    xhr = null;

  }
  return xhr;
}
