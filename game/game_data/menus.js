

function GenerateOptionUIs(){
  var optionsUI = {
		textcolor: rgba(0,0,0,0.75),
		y: 200,
		titlecolor: rgba(0,0,0,0.35),
		title: gameName,
		subtitle: "options",
		lines: [
      {
				left: "",
				center: "",
				right: ""
			},
			{
				left: "Accessibility",
				center: "",
				right: ""
			},
			{
				left: "",
				center: "Automation",
				right: "<none>"
			},
			{
				left: "",
				center: "Effects",
				right: "<all>"
			},
			{
				left: "",
				center: "Screenshake",
				right: "1"
			},
			{
				left: "",
				center: "",
				right: ""
			},
			{
				left: "",
				center: "back",
				cfunc: function(){
					gstate = states.splash;
				},
				right: ""
			}
		]
	}
  return optionsUI;
}