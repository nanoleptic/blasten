var bullets = [];
function nop(){}
function newBullet(x,y,a,spread,speed,options) {
  var options = options ? options : {};
  var aa = a+(Math.random()*2-1)*(Math.random()*2-1)*spread;
  for (var i = 0; i < effects.fireParticles; i++) {
    bparticle(
      x,
      y,
      aa+(Math.random()*2-1)*(Math.random()*2-1)*(Math.random()*2-1)
    )
  }
  var rx = Math.cos(aa);
  var ry = Math.sin(aa);
  recoilx -= rx*3;
  recoily -= ry*3;
  var bullet = {
    options: options,
    firstFrame: true,
    x: x,
    y: y,
    a: aa,
    da: Math.random()*Math.PI*2,
    speed: speed,
    spread: spread,
    startspeed: speed,
    prefix: options.prefix ? options.prefix : "",
    tag: options.tag ? options.tag : "weapon",
    size: options.size ? options.size : 8,
    rotate: options.rotate ? options.rotate : 0,
    slowdown: options.slowdown ? options.slowdown : 0,
    shape: options.shape ? options.shape : "line",
    color: options.color ? options.color : "white",
    onhit: options.onhit ? options.onhit : nop,
    onupdate: options.onupdate ? options.onupdate : nop
  }
  bullets.push(bullet);
  return bullet;
}

function updateBullets(dt) {
  var i = 0;
  var done = 0;
  while (i<bullets.length){
    var bullet = bullets[i];
    if (bullet.hasOwnProperty("purge") && bullet.purge) {// condition to remove
      bullets.splice(i,1);
    } else {
      done++;
      bullet.onupdate(bullet,dt);
      bullet.speed = lerp(bullet.speed, 0, bullet.slowdown*dt);
      bullet.a = bullet.a+=bullet.rotate*dt
      if (bullet.speed<100)
        bullet.purge = true;
      bullet.x += Math.cos(bullet.a)*bullet.speed*dt;
      bullet.y += Math.sin(bullet.a)*bullet.speed*dt;
      if (bullet.x<-100 || bullet.y<-100 || bullet.x>canvas.width+100 || bullet.y>canvas.height+100)
        bullet.purge = true;
      var hit = false;
      var enemyHit;
      for (var e=0; e<enemies.length; e++){
        var enemy = enemies[e];
        if (enemy.hp>0){
          var dx = enemy.x-bullet.x;
          var dy = enemy.y-bullet.y;
          var d2 = (dx*dx+dy*dy);
          var tdist = (enemy.size)
          if (d2<tdist*tdist){
            hit = true;
            enemyHit = enemy;
            enemy.hp--;
            enemy.hit = true;
            if (enemy.hp<=0){
              enemy.hitby = bullet;
              shotkills++;
            }
            bullet.purge=true;
            bullet.hit = true;
            explodeParticles(
              bullet.x,
              bullet.y,
              effects.hitBurnParticles,
              0,
              0.3,
              50,
              250
            );
            var aa = bullet.a+Math.PI+(Math.random()*2-1)*(Math.random()*2-1)*Math.PI/2;
            for (var bp = 0; bp < effects.hitSmokeParticles; bp++) {
              bparticle(
                bullet.x,
                bullet.y,
                aa+(Math.random()*2-1)*(Math.random()*2-1)*(Math.random()*2-1)
              );
            }
            break;
          }
        }
      }
      if (hit){
        bullet.onhit(bullet, enemyHit);
        if (bullet.hit)
          sound.thump.play();
        else
          i--;
      }
      i++;
    }
  }
  //console.log(done);
}

function drawBulletsFront() {
  for (var i=0; i<bullets.length; i++){
    var bullet = bullets[i];
    if (bullet.firstFrame || (bullet.hasOwnProperty("hit") && bullet.hit)){
      /*
      ctx.fillStyle = "white";
      circle(bullet.x,bullet.y,20);
      bullet.firstFrame = false;
      */
    } else {
      if (bullet.shape=="line"){
        var dx = Math.cos(bullet.a);
        var dy = Math.sin(bullet.a);
        var length = (bullet.size+2)*Math.min((bullet.speed/bullet.startspeed),1);
        ctx.strokeStyle = "white";
        ctx.lineWidth = (bullet.size-2)
        line(
          bullet.x-dx*(length-2),
          bullet.y-dy*(length-2),
          bullet.x+dx*(length-2),
          bullet.y+dy*(length-2)
        )
      } else {
        var size = (bullet.size)*Math.min((bullet.speed/bullet.startspeed),1)+2;
        ctx.fillStyle = "white";
        circle(bullet.x,bullet.y,size-2,null,bullet.da);
      }
    }
  }
}
function drawBulletsBack() {
  for (var i=0; i<bullets.length; i++){
    var bullet = bullets[i];
    bullet.firstFrame = bullet.firstFrame && effects.bulletFlash;
    if (effects.bulletFlash && (bullet.firstFrame || (bullet.hasOwnProperty("hit") && bullet.hit))){
      ctx.fillStyle = "white";
      circle(bullet.x,bullet.y,20,null,bullet.da);
      bullet.firstFrame = false;
    } else {
      if (bullet.shape=="line"){
        var dx = Math.cos(bullet.a);
        var dy = Math.sin(bullet.a);
        var length = (bullet.size+2)*Math.min((bullet.speed/bullet.startspeed),1);
        ctx.strokeStyle = bullet.color;
        ctx.lineWidth = (bullet.size+2)
        line(
          bullet.x-dx*length,
          bullet.y-dy*length,
          bullet.x+dx*length,
          bullet.y+dy*length
        )
      } else {
        var size = (bullet.size)*Math.min((bullet.speed/bullet.startspeed),1)+2;
        ctx.fillStyle = bullet.color;
        circle(bullet.x,bullet.y,size,null,bullet.da);
      }
    }
  }
}
