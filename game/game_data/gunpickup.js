var gunpickups = [];

var lastGun = -1;

var flashPick = 0;
var flashWep = 0;
var flashSpawn = 0;
var nextFlash = 0;

function newGunPickup(x,y) {
  flashPick = 1;
  flashSpawn = 1
  nextFlash = 1;
  if (lastGun==-1){
    lastGun = 0;
  } else {
    lastGun = (lastGun+Math.ceil(Math.random()*weaponNames.length*0.4))%weaponNames.length;
    //lastGun = (lastGun+1)%weaponNames.length;
  }
  var pickup = {
    x: x,
    y: y,
    wave: waveNumber,
    time: Math.floor(time*100)/100,
    weapon: weaponNames[lastGun]
  }
  gunpickups.push(pickup);
}

function updatePickups(dt) {
  var i = 0;
  flashPick = lerp(flashPick,0,2*dt)
  flashWep = lerp(flashWep,0,2*dt);
  flashSpawn = lerp(flashSpawn,0,2*dt);
  while (i<gunpickups.length){
    nextFlash-=dt*2;
    if (nextFlash<=0){
      flashPick = 1;
      nextFlash = 1;
    }
    var p = gunpickups[i];
    var dx = p.x-player.x;
    var dy = p.y-player.y;
    var d2 = (dx*dx+dy*dy)
    if (d2<(40*40)) {
      flashPick = 1;
      flashWep = 1;
      score++;
      sinceLast = 0;
      player.current = p.weapon;
      stats.weapons.push({
        wep: p.weapon,
        t: Math.floor(shaketime*100)/100,
        wav: waveNumber,
        s: p.wave
      });
      gunpickups.splice(i,1);
      tunnelRate -=20;
    } else {
      i++;
    }
  }
}


function drawPickups() {
    var style = wepColor();
    for (var i = 0; i < gunpickups.length; i++) {
      var p = gunpickups[i];
      var rad = 1-((shaketime*2)%1);
      if (effects.pickupPulse) {
        ctx.strokeStyle = style;
        ctx.lineWidth = lerp(8,0,rad);
        linecircle(p.x,p.y,rad*30,4,shaketime);
        ctx.lineWidth = lerp(15,0,rad);
        linecircle(p.x,p.y,rad*150,4,shaketime+rad);
      }
      ctx.fillStyle = "white";
      circle(p.x,p.y,15,4,shaketime);
    }
}
